package com.bot.buddybot;

import static org.assertj.core.api.Assertions.assertThat;

import com.bot.buddybot.core.diary.service.DiaryService;
import com.bot.buddybot.controller.BuddyBotController;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.bot.buddybot.core.fotokucing.FotoKucing;
import com.bot.buddybot.core.saran.SaranTempatRefreshing;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.MockitoAnnotations;
import com.linecorp.bot.model.event.source.UserSource;
import java.time.Instant;

@ExtendWith(MockitoExtension.class)
public class BuddyBotControllerTest {
	
	@Spy
	@InjectMocks
	private BuddyBotController controller;
	
	@Mock
    private LineMessagingClient lineMessagingClient;

	@Mock
	private DiaryService diaryService;
	
	public static MessageEvent<TextMessageContent> createDummyTextMessage(String text,
        String userId) {
        return new MessageEvent<>("replyToken", new UserSource(userId),
            new TextMessageContent("id", text),
            Instant.parse("2020-01-01T00:00:00.000Z"));
    }
	
	@Test
	public void contextLoads() throws Exception {
		assertThat(controller).isNotNull();
	}

	@Test
	public void handleInputTest(){
		MessageEvent<TextMessageContent> event;
		event = createDummyTextMessage("/halo","1");
		controller.handleTextEvent(event);
		Mockito.verify(controller).handleTextEvent(event);
		
		FotoKucing fotoKucing = new FotoKucing();
		String jawaban = fotoKucing.getFotoKucing();
		controller.convertStringToMessage(jawaban);
		Mockito.verify(controller).convertStringToMessage(jawaban);
		
		SaranTempatRefreshing saranTempat = new SaranTempatRefreshing();
		jawaban = saranTempat.getSaran();
		controller.convertStringToMessage(jawaban);
		Mockito.verify(controller).convertStringToMessage(jawaban);
		
		jawaban = "diaryflag1;Kesepian";
		controller.convertStringToMessage(jawaban);
		Mockito.verify(controller).convertStringToMessage(jawaban);
		
		jawaban = "diaryflag1;View;2";
		controller.convertStringToMessage(jawaban);
		Mockito.verify(controller).convertStringToMessage(jawaban);
	}
}
