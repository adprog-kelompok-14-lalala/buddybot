package com.bot.buddybot.core.response;

import com.bot.buddybot.core.response.IResponse;
import com.bot.buddybot.core.response.ResponseTidurAja;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestResponseTidurAja {
    IResponse test;

    @BeforeEach
    public void setUp(){
        test = new ResponseTidurAja();
    }

    @Test
    public void testGetResponse(){
        assertEquals("Mending tidur aja sih kak!", test.getResponse());
    }
}
