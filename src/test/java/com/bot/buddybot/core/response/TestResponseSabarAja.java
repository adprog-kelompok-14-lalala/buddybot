package com.bot.buddybot.core.response;

import com.bot.buddybot.core.response.IResponse;
import com.bot.buddybot.core.response.ResponseSabarAja;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestResponseSabarAja {
    IResponse test;

    @BeforeEach
    public void setUp(){
        test = new ResponseSabarAja();
    }

    @Test
    public void testResponseSabarAjaExist(){
        IResponse testExist = new ResponseSabarAja();

        assertNotNull(testExist);
    }

    @Test
    public void testGetResponse(){
        assertEquals("Tahanlah amarahmu. Coba tiduran dan pikirkan ulang segalanya.", test.getResponse());
    }
}