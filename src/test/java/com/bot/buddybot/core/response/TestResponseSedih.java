package com.bot.buddybot.core.response;

import com.bot.buddybot.core.response.IResponse;
import com.bot.buddybot.core.response.ResponseSedih;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class TestResponseSedih {
    IResponse test;

    @BeforeEach
    public void setUp(){
        test = new ResponseSedih();
    }

    @Test
    public void testGetResponse(){
        assertEquals("Nggak perlu sedihh!", test.getResponse());
    }
}
