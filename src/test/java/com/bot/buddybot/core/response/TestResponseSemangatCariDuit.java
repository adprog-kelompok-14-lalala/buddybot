package com.bot.buddybot.core.response;

import com.bot.buddybot.core.response.IResponse;
import com.bot.buddybot.core.response.ResponseSemangatCariDuit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestResponseSemangatCariDuit {
    IResponse test;

    @BeforeEach
    public void setUp(){
        test = new ResponseSemangatCariDuit();
    }

    @Test
    public void testGetResponse(){
        assertEquals("Semangat cari rezekinya! Kekayaan datang pada orang yang mau membenahi dirinya sendiri.\n", test.getResponse());
    }
}
