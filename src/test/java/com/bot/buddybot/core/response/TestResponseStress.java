package com.bot.buddybot.core.response;

import com.bot.buddybot.core.response.IResponse;
import com.bot.buddybot.core.response.ResponseStress;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class TestResponseStress {
    IResponse test;

    @BeforeEach
    public void setUp(){
        test = new ResponseStress();
    }

    @Test
    public void testGetResponse(){
        assertEquals("Semua pasti berlalu kok!", test.getResponse());
    }
}
