package com.bot.buddybot.core.mood;

import com.bot.buddybot.core.fotokucing.NoFoto;
import com.bot.buddybot.core.mood.Mood;
import com.bot.buddybot.core.mood.Sedih;
import com.bot.buddybot.core.quotes.QuotesHappy;
import com.bot.buddybot.core.response.ResponseSedih;
import com.bot.buddybot.core.saran.SaranVideoLucu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class TestSedih {
    Mood test;
    String response;
    ArrayList<String> kumpulanLink;
    ArrayList<String> kumpulanQuotesHappy;

    @BeforeEach
    public void setUp(){
        test = new Sedih();
        response = response = "Perintah yang tersedia \n" +
                "/getHappyQuotes - Quotes-Quotes happy untuk kamu yang lagi sedih \n" +
                "/getSaranVideoLucu - Link-Link video untuk kamu yang ingin ketawa \n" +
                "/mauNulis - Menulis diary \n" +
                "/bacaDiary - Membaca diary \n" +
                "/gantiMood - Jika rasa kamu sudah berubah";
        kumpulanLink = new ArrayList<>(Arrays.asList(
                "https://www.youtube.com/watch?v=PI4JABNwB_U",
                "https://www.youtube.com/watch?v=5jpkxyaA_YI",
                "https://www.youtube.com/watch?v=5DzTwbyBhEc",
                "https://www.youtube.com/watch?v=VWWUP1vzcuU",
                "https://www.youtube.com/watch?v=mxn9Zdlqv_A",
                "https://www.youtube.com/watch?v=SaS4hjbJa1U",
                "https://www.youtube.com/watch?v=JHy6bBKu0j4"
        ));
        kumpulanQuotesHappy = new ArrayList<>(Arrays.asList(
			"\"Nothing always stay the same. "
			+"You don't stay happy forever. "
			+"You don't stay sad forever.\" -Cat Zingano",
			"\"It will get better. It maybe stormy now but it can't rain forever.\"",
			"\"A certain darkness is needed to see the stars.\"",
			"\"It's okay to be sad. It's okay to cry. Just don't let the sadness"
			+"take over your life. You will be smiling soon.\"",
			"\"Someday, everything will make perfect sense. Everything happens for a reason.\"",
			"\"Happiness comes in waves. It'll find you again.\"",
			"\"Some days are just bad days, that's all. You have to experience "
			+ "sadness to know happiness. Remind yourself, not every day "
			+ "is going to be a good day. That's just the way it is!\""
		));
    }

    @Test
    public void testSedihGetHelp(){
        assertEquals(response,test.getHelp());
    }

    @Test
    public void testGetQuotes() {
        String quotes = test.getQuotes();
        assertTrue(kumpulanQuotesHappy.contains(quotes));
    }

    @Test
    public void testGetSaran(){
        String[] saran = test.getSaran().split(": ");
        String link = saran[saran.length-1];
        assertTrue(kumpulanLink.contains(link));
    }

    @Test
    public void testGetResponses() {
        assertEquals("Nggak perlu sedihh!", test.getResponses());
    }

    @Test
    public void testSetQuotes() {
        test.setQuote(new QuotesHappy());
        String quotes = test.getQuotes();
        assertTrue(kumpulanQuotesHappy.contains(quotes));
    }

    @Test
    public void testSetFotoKucing() {
        test.setFoto(new NoFoto());
        assertNull(test.getFotoKucing());
    }

    @Test
    public void testSetSaran() {
        test.setSaran(new SaranVideoLucu());
        String[] saran = test.getSaran().split(": ");
        String link = saran[saran.length - 1];
        assertTrue(kumpulanLink.contains(link));
    }

    @Test
    public void testSetResponse(){
        test.setResponse(new ResponseSedih());
        assertEquals("Nggak perlu sedihh!", test.getResponses());
    }
	
	@Test
	public void testGetQuickReplyHelp(){
		String output = test.getQuickReplyHelp();
		assertEquals("help getHappyQuotes getSaranVideoLucu mauNulis bacaDiary gantiMood", output);
	}
}
