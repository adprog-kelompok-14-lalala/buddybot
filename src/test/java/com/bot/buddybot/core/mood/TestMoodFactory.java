package com.bot.buddybot.core.mood;

import com.bot.buddybot.core.mood.Mood;
import com.bot.buddybot.core.mood.MoodFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestMoodFactory {
    Mood mood;

    @BeforeEach
    public void setUp(){
        // EMPTY
    }

    @Test
    public void testCreateFactory(){
        mood = MoodFactory.createMood("/sedih");
        assertEquals("com.bot.buddybot.core.mood.Sedih", mood.getClass().getName());
        mood = MoodFactory.createMood("/marah");
        assertEquals("com.bot.buddybot.core.mood.Marah", mood.getClass().getName());
        mood = MoodFactory.createMood("/miskin");
        assertEquals("com.bot.buddybot.core.mood.Miskin", mood.getClass().getName());
        mood = MoodFactory.createMood("/kesepian");
        assertEquals("com.bot.buddybot.core.mood.Kesepian", mood.getClass().getName());
        mood = MoodFactory.createMood("/stres");
        assertEquals("com.bot.buddybot.core.mood.Stress", mood.getClass().getName());
    }
}
