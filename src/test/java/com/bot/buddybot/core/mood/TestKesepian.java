package com.bot.buddybot.core.mood;

import com.bot.buddybot.core.mood.Mood;
import com.bot.buddybot.core.mood.Kesepian;
import com.bot.buddybot.core.fotokucing.FotoKucing;
import com.bot.buddybot.core.quotes.NoQuotes;
import com.bot.buddybot.core.response.ResponseTidurAja;
import com.bot.buddybot.core.saran.SaranTempatRefreshing;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestKesepian {
    Mood test;
    String response;
	ArrayList<String> kumpulanSaran;

    @BeforeEach
    public void setUp(){
        test = new Kesepian();
        response = "Perintah yang tersedia \n" +
                "/getFotoKucing - Foto-Foto kucing untuk kamu yang sedang kesepian \n" +
                "/getTempatRefreshing - Link-Link video untuk kamu yang ingin ketawa \n" +
                "/mauNulis - Menulis diary \n" +
				"/bacaDiary - Membaca diary \n" +
				"/gantiMood - Jika rasa kamu sudah berubah";
		kumpulanSaran = new ArrayList<>(Arrays.asList(
				"https://i1.wp.com/www.jajanbeken.com/wp-content/uploads/2018/12/jajanbeken-bogor-botanical-garden-entrance.jpg?fit=1199%2C673;"
						+ "Kebun Raya Bogor;Bogor",
				"https://media.suara.com/pictures/original/2019/12/07/90846-jerapa.jpg;Taman Safari Indonesia;Cisarua",
				"https://upload.wikimedia.org/wikipedia/commons/7/78/Ragunan_Zoo_Gate.jpg;Kebun Binatang Ragunan;Pasar Minggu",
				"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS8lzE1CXiE8dxgwUxfVRdnrpDsxpNGHkJiAIIisXhIbR7u8Alp&usqp=CAU;"
						+ "Perpustakaan Nasional;Jakarta Pusat",
				"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSZytoEP7LBedLgT00tQYaGdwIvC0gcPsDiPcMIJDf6gOUx16fn&usqp=CAU;"
						+ "Cimory Riverside;Cisarua",
				"https://anekatempatwisata.com/wp-content/uploads/2014/08/Kawah-Putih-Bandung.jpg;Kawah Putih;Bandung"
		));
    }

    @Test
    public void testGetHelp(){
        assertEquals(response, test.getHelp());
    }
	
	@Test
	public void testGetQuotes(){
		assertEquals(test.getQuotes(),"");
	}
	
	@Test
	public void testGetSaran(){
		String strLink = "refreshflag" + String.join(",", kumpulanSaran);
		assertEquals(strLink, test.getSaran());
	}
	
	@Test
	public void testGetResponses(){
		assertEquals("Mending tidur aja sih kak!", test.getResponses());
	}
	
	@Test
	public void testSetQuote(){
		test.setQuote(new NoQuotes());
		assertEquals(test.getQuotes(),"");
    }
	
	@Test
	public void testSetFotoKucing(){
		test.setFoto(new FotoKucing());
		assertNotNull(test.getFotoKucing());
	}
	
	@Test
    public void testSetSaran(){
        test.setSaran(new SaranTempatRefreshing());
		String strLink = "refreshflag" + String.join(",", kumpulanSaran);
		assertEquals(strLink, test.getSaran());
    }
	
	@Test
    public void testSetResponse() {
		test.setResponse(new ResponseTidurAja());
		assertEquals("Mending tidur aja sih kak!", test.getResponses());
    }
	
	@Test
	public void testGetQuickReplyHelp(){
		String output = test.getQuickReplyHelp();
		assertEquals("help getFotoKucing getTempatRefreshing mauNulis bacaDiary gantiMood", output);
	}
}
