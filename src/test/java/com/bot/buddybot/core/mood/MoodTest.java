package com.bot.buddybot.core.mood;

import com.bot.buddybot.core.fotokucing.IFotoKucing;
import com.bot.buddybot.core.quotes.IQuote;
import com.bot.buddybot.core.response.IResponse;
import com.bot.buddybot.core.saran.ISaran;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MoodTest {

    private Class<?> moodClass;

    @BeforeEach
    public void setUp() throws Exception{
        moodClass = Class.forName("com.bot.buddybot.core.mood.Mood");
    }

    @Test
    public void testMoodIsAbstract(){
        assertTrue(Modifier.isAbstract((moodClass.getModifiers())));
    }

    @Test
    public void testMoodHasGetQuotesMethod() throws Exception{
        Method getQuote = moodClass.getDeclaredMethod("getQuotes");

        assertTrue(Modifier.isPublic((getQuote.getModifiers())));
        assertEquals(0, getQuote.getParameterCount());
        assertEquals("java.lang.String", getQuote.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMoodHasGetSaranMethod() throws Exception{
        Method getSaran = moodClass.getDeclaredMethod("getSaran");

        assertTrue(Modifier.isPublic((getSaran.getModifiers())));
        assertEquals(0, getSaran.getParameterCount());
        assertEquals("java.lang.String", getSaran.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMoodHasGetFotoKucingMethod() throws Exception{
        Method getFotoKucing = moodClass.getDeclaredMethod("getFotoKucing");

        assertTrue(Modifier.isPublic((getFotoKucing.getModifiers())));
        assertEquals(0, getFotoKucing.getParameterCount());
        assertEquals("java.lang.String", getFotoKucing.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMoodHasGetResponsesMethod() throws Exception{
        Method getResponses = moodClass.getDeclaredMethod("getResponses");

        assertTrue(Modifier.isPublic((getResponses.getModifiers())));
        assertEquals(0, getResponses.getParameterCount());
        assertEquals("java.lang.String", getResponses.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMoodHasQuotesSetter() throws Exception{
        Method setQuotes = moodClass.getDeclaredMethod("setQuote", IQuote.class);

        assertTrue(Modifier.isPublic((setQuotes.getModifiers())));
        assertEquals(1, setQuotes.getParameterCount());
        assertEquals("void", setQuotes.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMoodHasSaranSetter() throws Exception{
        Method setSaran = moodClass.getDeclaredMethod("setSaran", ISaran.class);

        assertTrue(Modifier.isPublic((setSaran.getModifiers())));
        assertEquals(1, setSaran.getParameterCount());
        assertEquals("void", setSaran.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMoodHasFotoKucingSetter() throws Exception{
        Method setFotoKucing = moodClass.getDeclaredMethod("setFoto", IFotoKucing.class);

        assertTrue(Modifier.isPublic((setFotoKucing.getModifiers())));
        assertEquals(1, setFotoKucing.getParameterCount());
        assertEquals("void", setFotoKucing.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMoodHasResponsesSetter() throws Exception{
        Method setResponses = moodClass.getDeclaredMethod("setResponse", IResponse.class);

        assertTrue(Modifier.isPublic((setResponses.getModifiers())));
        assertEquals(1, setResponses.getParameterCount());
        assertEquals("void", setResponses.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMoodHasQuotesGetter() throws Exception{
        Method getQuotes = moodClass.getDeclaredMethod("getQuotes");

        assertTrue(Modifier.isPublic((getQuotes.getModifiers())));
        assertEquals(0, getQuotes.getParameterCount());
        assertEquals(String.class, getQuotes.getGenericReturnType());
    }

    @Test
    public void testMoodHasSaranGetter() throws Exception{
        Method getSaran = moodClass.getDeclaredMethod("getSaran");

        assertTrue(Modifier.isPublic((getSaran.getModifiers())));
        assertEquals(0, getSaran.getParameterCount());
        assertEquals(String.class, getSaran.getGenericReturnType());
    }

    @Test
    public void testMoodHasFotoKucingGetter() throws Exception{
        Method getFotoKucing = moodClass.getDeclaredMethod("getFotoKucing");

        assertTrue(Modifier.isPublic((getFotoKucing.getModifiers())));
        assertEquals(0, getFotoKucing.getParameterCount());
        assertEquals(String.class, getFotoKucing.getGenericReturnType());
    }

    @Test
    public void testMoodHasResponsesGetter() throws Exception{
        Method getResponses = moodClass.getDeclaredMethod("getResponses");

        assertTrue(Modifier.isPublic((getResponses.getModifiers())));
        assertEquals(0, getResponses.getParameterCount());
        assertEquals(String.class, getResponses.getGenericReturnType());
    }
}
