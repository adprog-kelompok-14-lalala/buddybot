package com.bot.buddybot.core.mood;

import com.bot.buddybot.core.fotokucing.NoFoto;
import com.bot.buddybot.core.mood.Mood;
import com.bot.buddybot.core.mood.Stress;
import com.bot.buddybot.core.quotes.QuotesPantangMenyerah;
import com.bot.buddybot.core.response.ResponseStress;
import com.bot.buddybot.core.saran.SaranTempatRefreshing;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class TestStress {
    Mood test;
    String response;
    ArrayList<String> kumpulanQuotesPantangMenyerah;
    ArrayList<String> kumpulanSaran;

    @BeforeEach
    public void setUp(){
        test = new Stress();
        response = "Perintah yang tersedia \n" +
                "/getQuotesTetapKuat - Quotes-Quotes untuk kamu yang ingin menyerah \n" +
                "/getTempatRefreshing - Saran-Saran tempat untuk kamu yang ingin refreshing \n" +
                "/mauNulis - Menulis diary \n" +
                "/bacaDiary - Membaca diary \n" +
                "/gantiMood - Jika rasa kamu sudah berubah";
        kumpulanQuotesPantangMenyerah = new ArrayList<>(Arrays.asList(
			"\"Just because we're in a stressful situation, "
			+"doesn't mean that we have to get stressed out. "
			+"You may be in the storm. The key is, "
			+"don't let the storm get in you.\" -Joel Oesteen",
			"\"Don't be stressed over what we can't control.\"",
			"\"Breathe in, breathe out.\"",
			"\"Don't stress, do your best, forget the rest.\"",
			"\"Stressed spelled backwards is desserts.\" -Loretta Laroche",
			"\"Your calm mind is the ultimate weapon against your challenges. "
			+ "So relax.\" -Bryant McGill",
			"\"You can do this!\""
		));
        kumpulanSaran = new ArrayList<>(Arrays.asList(
                "https://i1.wp.com/www.jajanbeken.com/wp-content/uploads/2018/12/jajanbeken-bogor-botanical-garden-entrance.jpg?fit=1199%2C673;"
                        + "Kebun Raya Bogor;Bogor",
                "https://media.suara.com/pictures/original/2019/12/07/90846-jerapa.jpg;Taman Safari Indonesia;Cisarua",
                "https://upload.wikimedia.org/wikipedia/commons/7/78/Ragunan_Zoo_Gate.jpg;Kebun Binatang Ragunan;Pasar Minggu",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS8lzE1CXiE8dxgwUxfVRdnrpDsxpNGHkJiAIIisXhIbR7u8Alp&usqp=CAU;"
                        + "Perpustakaan Nasional;Jakarta Pusat",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSZytoEP7LBedLgT00tQYaGdwIvC0gcPsDiPcMIJDf6gOUx16fn&usqp=CAU;"
                        + "Cimory Riverside;Cisarua",
                "https://anekatempatwisata.com/wp-content/uploads/2014/08/Kawah-Putih-Bandung.jpg;Kawah Putih;Bandung"
        ));
    }

    @Test
    public void testStressGetHelp(){
        assertEquals(response,test.getHelp());
    }

    @Test
    public void testGetQuotes() {
        String quotes = test.getQuotes();
        assertTrue(kumpulanQuotesPantangMenyerah.contains(quotes));
    }

    @Test
    public void testGetSaran(){
        String strLink = "refreshflag" + String.join(",", kumpulanSaran);
        assertEquals(strLink, test.getSaran());
    }

    @Test
    public void testGetResponses() {
        assertEquals("Semua pasti berlalu kok!", test.getResponses());
    }

    @Test
    public void testSetQuotes() {
        test.setQuote(new QuotesPantangMenyerah());
        String quotes = test.getQuotes();
        assertTrue(kumpulanQuotesPantangMenyerah.contains(quotes));
    }

    @Test
    public void testSetFotoKucing() {
        test.setFoto(new NoFoto());
        assertNull(test.getFotoKucing());
    }

    @Test
    public void testSetSaran() {
        test.setSaran(new SaranTempatRefreshing());
        String strLink = "refreshflag" + String.join(",", kumpulanSaran);
        assertEquals(strLink, test.getSaran());
    }

    @Test
    public void testSetResponse() {
        test.setResponse(new ResponseStress());
        assertEquals("Semua pasti berlalu kok!", test.getResponses());
    }
	
	@Test
	public void testGetQuickReplyHelp(){
		String output = test.getQuickReplyHelp();
		assertEquals("help getQuotesTetapKuat getTempatRefreshing mauNulis bacaDiary gantiMood", output);
	}
}
