package com.bot.buddybot.core.mood;

import com.bot.buddybot.core.mood.Mood;
import com.bot.buddybot.core.mood.Miskin;
import com.bot.buddybot.core.quotes.NoQuotes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestMiskin {
    Mood test;
    String response;

    @BeforeEach
    public void setUp(){
        test = new Miskin();
        response = "Perintah yang tersedia \n" +
                "/getSaranMagang - Link-Link magang untuk kamu yang ingin punya duit \n" +
                "/mauNulis - Menulis diary \n" +
                "/bacaDiary - Membaca diary \n" +
                "/gantiMood - Jika rasa kamu sudah berubah";
    }

    @Test
    public void testGetHelp(){
        assertEquals(response, test.getHelp());
    }

    @Test
    public void testGetResponses(){
        assertEquals("Semangat cari rezekinya! Kekayaan datang pada orang yang mau membenahi dirinya sendiri.\n", test.getResponses());
    }

    @Test
    public void testGetFotoKucing(){
        assertNull(test.getFotoKucing());
    }


    @Test
    public void testGetSaranMagang(){
        assertEquals("Lagi butuh duit ya?:( Tinggal dicari aja kak! \nWebsite-website lowongan magang:"
                +"\n- https://www.kalibrr.id/"
                +"\n- https://magang.id/"
                +"\n- https://magang.co.id"
                +"\n- https://www.loker.id/tipe-pekerjaan/magang"
                +"\n- https://www.jobstreet.co.id/en/job-search/magang-jobs/"
                +"\n- http://studentjob.co.id//"
                +"\n- https://www.topkarir.com/lowongan/magang"
                +"\n- https://www.careerjet.co.id/"
                +"\n SEMANGAT <3", test.getSaran());
    }

    @Test
    public void testGetQuotes(){
        assertEquals(test.getQuotes(),"");
    }

    @Test
    public void testSetQuote(){
        test.setQuote(new NoQuotes());
        assertEquals(test.getQuotes(),"");
    }
	
	@Test
	public void testGetQuickReplyHelp(){
		String output = test.getQuickReplyHelp();
		assertEquals("help getSaranMagang mauNulis bacaDiary gantiMood", output);
	}
}
