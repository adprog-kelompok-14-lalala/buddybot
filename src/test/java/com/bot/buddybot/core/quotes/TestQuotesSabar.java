package com.bot.buddybot.core.quotes;

import com.bot.buddybot.core.quotes.IQuote;
import com.bot.buddybot.core.quotes.QuotesSabar;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestQuotesSabar {
    IQuote test;
    ArrayList<String> kumpulanQuotesSabar;

    @BeforeEach
    public void setUp(){
        test = new QuotesSabar();
        kumpulanQuotesSabar = new ArrayList<> (Arrays.asList(
                "Yang sabar yaa. Sesungguhnya setalah kesulitan ada kemudahan.",
                "Untuk mencintai, kamu harus memiliki kekuatan. Kekuatan melawan amarah dengan kesabaran dan kekuatan memaafkan dengan ketulusan. – Mario Teguh",
                "“Ternyata, banyak hal yang tidak selesai dengan amarah.” – Iwan Fals",
                "“Cinta dan kelembutan adalah sifat manusia, amarah dan gairah nafsu adalah sifat binatang.” – Jalaluddin Rumi",
                "“Saat kau membalas kebencian dengan amarah dan caci maki, saat itulah musuhmu menang.” – Andy F. Noya",
                "“Siapakah orang yang kurang ilmu? dialah orang yang mengandalkan otot dan amarah dalam menyikapi segala sesuatu.” – Abdullah Gymnastiar",
                "“Ketika Anda marah, hitunglah sampai sepuluh sebelum Anda berbicara. Jika Anda sangat marah, hitunglah sampai seratus.” – Thomas Jefferson",
                " “Siapapun bisa marah, karena marah itu mudah. Tetapi marah kepada siapa, dengan kadar kemarahan yang pas, pada saat dan tujuan yang tepat, serta dengan cara benar itu yang sulit.” – Aristoteles",
                "“Lemparkan senyum kepada yang memancing amarah.” – Harry Slyman"
        ));
    }

    @Test
    public void testQuotesSabarExist(){
        IQuote testExist = new QuotesSabar();
        assertNotNull(testExist);
    }

    @Test
    public void testGetQuotes(){
        assertTrue(kumpulanQuotesSabar.contains(test.getQuotes()));
    }
}
