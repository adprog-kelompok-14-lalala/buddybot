package com.bot.buddybot.core.quotes;

import com.bot.buddybot.core.quotes.IQuote;
import com.bot.buddybot.core.quotes.QuotesPantangMenyerah;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class TestQuotesPantangMenyerah {
    IQuote test;
    ArrayList<String> kumpulanQuotesPantangMenyerah;

    @BeforeEach
    public void setUp(){
        test = new QuotesPantangMenyerah();
        kumpulanQuotesPantangMenyerah = new ArrayList<>(Arrays.asList(
			"\"Just because we're in a stressful situation, "
			+"doesn't mean that we have to get stressed out. "
			+"You may be in the storm. The key is, "
			+"don't let the storm get in you.\" -Joel Oesteen",
			"\"Don't be stressed over what we can't control.\"",
			"\"Breathe in, breathe out.\"",
			"\"Don't stress, do your best, forget the rest.\"",
			"\"Stressed spelled backwards is desserts.\" -Loretta Laroche",
			"\"Your calm mind is the ultimate weapon against your challenges. "
			+ "So relax.\" -Bryant McGill",
			"\"You can do this!\""
		));
    }

    @Test
    public void testQuotesPantangMenyerahExist(){
        IQuote testExist = new QuotesPantangMenyerah();
        assertNotNull(testExist);
    }

    @Test
    public void testGetQuotes(){
        String quotes = test.getQuotes();
        assertTrue(kumpulanQuotesPantangMenyerah.contains(quotes));
    }
}
