package com.bot.buddybot.core.quotes;

import com.bot.buddybot.core.quotes.IQuote;
import com.bot.buddybot.core.quotes.QuotesHappy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;


public class TestQuotesHappy {
    IQuote test;
    ArrayList<String> kumpulanQuotesHappy;

    @BeforeEach
    public void setUp(){
        test = new QuotesHappy();
        kumpulanQuotesHappy = new ArrayList<>(Arrays.asList(
			"\"Nothing always stay the same. "
			+"You don't stay happy forever. "
			+"You don't stay sad forever.\" -Cat Zingano",
			"\"It will get better. It maybe stormy now but it can't rain forever.\"",
			"\"A certain darkness is needed to see the stars.\"",
			"\"It's okay to be sad. It's okay to cry. Just don't let the sadness"
			+"take over your life. You will be smiling soon.\"",
			"\"Someday, everything will make perfect sense. Everything happens for a reason.\"",
			"\"Happiness comes in waves. It'll find you again.\"",
			"\"Some days are just bad days, that's all. You have to experience "
			+ "sadness to know happiness. Remind yourself, not every day "
			+ "is going to be a good day. That's just the way it is!\""
		));
    }

    @Test
    public void testQuotesHappyExist(){
        IQuote testExist = new QuotesHappy();
        assertNotNull(testExist);
    }

    @Test
    public void testGetQuotes(){
        String quotes = test.getQuotes();
        assertTrue(kumpulanQuotesHappy.contains(quotes));
    }
}


