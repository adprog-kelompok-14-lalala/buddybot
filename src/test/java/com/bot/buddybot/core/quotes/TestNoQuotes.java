package com.bot.buddybot.core.quotes;

import com.bot.buddybot.core.quotes.IQuote;
import com.bot.buddybot.core.quotes.NoQuotes;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestNoQuotes {
    IQuote test;

    @BeforeEach
    public void setUp(){
        test = new NoQuotes();
    }

    @Test
    public void testNoQuotesExist(){
        IQuote testExist = new NoQuotes();
        assertNotNull(testExist);
    }

    @Test
    public void testGetQuotes(){
        assertEquals(test.getQuotes(),"");
    }
}
