package com.bot.buddybot.core.flexmessage;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.bot.buddybot.core.diary.core.Diary;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import java.util.List;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;


public class DiaryListMessageTest {

	@Test
	public void testGetMessageMethod() {
		DiaryListMessage diaryListMessage = new DiaryListMessage();
		Diary diary = new Diary(1, "test", "test", "Kesepian");
		List<Diary> diaryList = new ArrayList<>();
		diaryList.add(diary);
		assertEquals("lists of diary you made", diaryListMessage.get(diaryList, QuickReply.builder().build()).getAltText());
	}
}
