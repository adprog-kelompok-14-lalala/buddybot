package com.bot.buddybot.core.flexmessage;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.bot.buddybot.core.diary.core.Diary;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import org.junit.jupiter.api.Test;


public class DiaryViewMessageTest {

	@Test
	public void testGetMessageMethod() {
		DiaryViewMessage diaryViewMessage = new DiaryViewMessage();
		Diary diary = new Diary(1, "test", "test", "Kesepian");
		assertEquals("details of the diary you made", diaryViewMessage.get(diary, QuickReply.builder().build()).getAltText());
	}
}
