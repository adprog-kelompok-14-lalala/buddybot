package com.bot.buddybot.core.scheduler;
import com.bot.buddybot.controller.BuddyBotController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class TestGreeting {
    Greeting test;
    BuddyBotController buddyBotController;

    @BeforeEach
    public void setUp(){test = new Greeting(buddyBotController);}

    @Test
    public void testRandomGreetings(){
        ArrayList<String> kumpulanGreetings = new ArrayList<>(Arrays.asList(
                "SELAMAT PAGI TEMAN BUDDY BOT! SEMANGAT MENJALANI HARI INI YA",
                "SELAMAT PAGI! AWALI HARI INI DENGAN SENYUMAN",
                "SELAMAT PAGI, SEMOGA PAGI YANG CERAH INI AKAN MENGANTARKAN "
                    + "HARI-HARIMU MENJADI LEBIH INDAH",
                "PAGI MERUPAKAN JALAN UNTUK MELIHAT DUNIA BARU, AYO BANGUN!",
                "SAMBUT PAGI DENGAN SENYUMAN DAN SEMANGAT. SELAMAT PAGI!"
        ));
        String testGreeting = test.getRandomChat();

        assertTrue(kumpulanGreetings.contains(testGreeting));
    }

}
