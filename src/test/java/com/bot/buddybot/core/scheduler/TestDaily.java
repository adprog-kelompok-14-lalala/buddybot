package com.bot.buddybot.core.scheduler;
import com.bot.buddybot.controller.BuddyBotController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class TestDaily {
    Daily test;
    BuddyBotController buddyBotController;

    @BeforeEach
    public void setUp(){test = new Daily(buddyBotController);}

    @Test
    public void testRandomQuotes(){
        ArrayList<String> kumpulanDailyQuotes = new ArrayList<>(Arrays.asList(
                "Berhenti Menyalahkan Segalanya",
                "Ambil Risiko, Bermimpi Lebih Besar, dan Berharap Besar",
                "Kerjakan dengan Lebih dan Sepenuh Hati",
                "Lakukan Apa yang Membuatmu Bahagia",
                "Jangan Pernah Menyerah Apapun yang Terjadi",
                "Syukuri dan Hargai  Hal-Hal yang Anda Miliki",
                "Nikmati dan Hargai Perubahan dalam Kehidupan",
                "Keraguan adalah Musuh Terbesar dalam Meraih Mimpi",
                "Memaafkan Membuat Anda Menjadi Pribadi yang Semakin Kuat",
                "Selalu Lakukan Perubahan Kecil ke Arah yang Lebih Baik",
                "Berhentilah Hanya Ketika Anda Berhasil Mengalahkan Tantangan"
        ));
        String testQuote = test.getRandomChat();

        assertTrue(kumpulanDailyQuotes.contains(testQuote));
    }
	
	@Test
	public void testRun(){
		test.run();
	}

}
