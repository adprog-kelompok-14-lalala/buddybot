package com.bot.buddybot.core.scheduler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestBroadcastChat {

    private Class<?> broadcast;

    @BeforeEach
    public void setUp() throws Exception{
        broadcast = Class.forName("com.bot.buddybot.core.scheduler.BroadcastChat");
    }

    @Test
    public void testClassIsAbstract(){
        assertTrue(Modifier.isAbstract((broadcast.getModifiers())));
    }

    @Test
    public void testHasGetRandomChatMethod() throws Exception{
        Method getChat = broadcast.getDeclaredMethod("getRandomChat");

        assertTrue(Modifier.isPublic((getChat.getModifiers())));
        assertEquals(0, getChat.getParameterCount());
        assertEquals("java.lang.String", getChat.getGenericReturnType().getTypeName());
    }

}
