package com.bot.buddybot.core.saran;

import com.bot.buddybot.core.saran.ISaran;
import com.bot.buddybot.core.saran.SaranVideoLucu;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestSaranVideoLucu {
    ISaran test;
    ArrayList<String> kumpulanLink;

    @BeforeEach
    public void setUp(){
        test = new SaranVideoLucu();
        kumpulanLink = new ArrayList<>(Arrays.asList(
                "https://www.youtube.com/watch?v=PI4JABNwB_U",
                "https://www.youtube.com/watch?v=5jpkxyaA_YI",
                "https://www.youtube.com/watch?v=5DzTwbyBhEc",
                "https://www.youtube.com/watch?v=VWWUP1vzcuU",
                "https://www.youtube.com/watch?v=mxn9Zdlqv_A",
                "https://www.youtube.com/watch?v=SaS4hjbJa1U",
                "https://www.youtube.com/watch?v=JHy6bBKu0j4"
        ));
    }

    @Test
    public void getSaranTest(){
        String[] video = test.getSaran().split(": ");
        String link = video[video.length-1];
        assertTrue(kumpulanLink.contains(link));
    }
}
