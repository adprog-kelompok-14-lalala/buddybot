package com.bot.buddybot.core.saran;

import com.bot.buddybot.core.saran.ISaran;
import com.bot.buddybot.core.saran.SaranMagang;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestSaranMagang {
    ISaran test;
    String response;

    @BeforeEach
    public void setUp(){
        test = new SaranMagang();
        response = "Lagi butuh duit ya?:( Tinggal dicari aja kak! \nWebsite-website lowongan magang:"
                +"\n- https://www.kalibrr.id/"
                +"\n- https://magang.id/"
                +"\n- https://magang.co.id"
                +"\n- https://www.loker.id/tipe-pekerjaan/magang"
                +"\n- https://www.jobstreet.co.id/en/job-search/magang-jobs/"
                +"\n- http://studentjob.co.id//"
                +"\n- https://www.topkarir.com/lowongan/magang"
                +"\n- https://www.careerjet.co.id/"
                +"\n SEMANGAT <3";
    }

    @Test
    public void getSaranTest(){
        assertEquals(response, test.getSaran());
    }
}
