package com.bot.buddybot.core.saran;

import com.bot.buddybot.core.saran.ISaran;
import com.bot.buddybot.core.saran.NoSaran;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class TestNoSaran {
    ISaran test;

    @BeforeEach
    public void setUp(){
        test = new NoSaran();
    }

    @Test
    public void testNoSaranExists(){
        ISaran testExists = new NoSaran();

        assertNotNull(testExists);
    }

    @Test
    public void testGetSaran(){
        assertNull(test.getSaran());
    }
}
