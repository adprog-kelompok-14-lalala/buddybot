package com.bot.buddybot.core.diary;

import com.bot.buddybot.core.diary.core.Diary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DiaryTest {

    Diary diary;

    @BeforeEach
    public void setUp(){
		diary = new Diary();
        diary = new Diary(1, "Usedih", "sedih", "Sedih");
    }

    @Test
    public void testGetterMethod(){
       assertEquals(diary.getMessageId(), 1);
       assertEquals(diary.getUserId(), "Usedih");
       assertEquals(diary.getMood(), "Sedih");
       assertEquals(diary.getDiaryText(), "sedih");
    }

    @Test
    public void testSetterMethod(){
        diary.setUserId("sachiko");
        diary.setMood("Marah");
        diary.setDiaryText("marah");

        assertEquals(diary.getMessageId(), 1);
        assertEquals(diary.getUserId(), "sachiko");
        assertEquals(diary.getMood(), "Marah");
        assertEquals(diary.getDiaryText(), "marah");
		assertTrue(diary.get() instanceof Diary);
		String expectedOutput = "diary id : " + diary.getMessageId() + " | " + diary.getMood() + "\n" 
			   + "-----------------------------------------------\n"
		       + diary.getDiaryText() 
			   + "o---------------------------------------------o\n";
		assertTrue(expectedOutput.equals(diary.toString()));
    }
}
