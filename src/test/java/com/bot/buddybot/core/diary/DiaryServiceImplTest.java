package com.bot.buddybot.core.diary;

import com.bot.buddybot.core.diary.service.DiaryService;
import com.bot.buddybot.core.diary.core.Diary;
import java.util.List;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class DiaryServiceImplTest {

    @Autowired
    private DiaryService diaryService;

    @Test
    public void whenFindAllIsCalledItShouldCallFindAll(){
		Diary diary = new Diary(4, "Usedih", "sedih", "Sedih");
		diaryService.register(diary);
        List<Diary> listOutput = diaryService.findAll();
		assertNotEquals(listOutput.size(), 0);
		diaryService.erase(4);
    }

    @Test
    public void whenFindDiaryIsCalledItShouldCallFindById(){
		Diary diary = new Diary(4, "Usedih", "sedih", "Sedih");
		diaryService.register(diary);
        Diary diaryOutput = diaryService.findWithMessageId(new Long(4));
		assertEquals(diaryOutput.getMessageId(), 4);
		diaryService.erase(4);
    }
	
	@Test
    public void whenRegisterIsCalledItShouldCallSave(){
        Diary diary = new Diary(1, "Usedih", "sedih", "Sedih");
        diaryService.register(diary);
		List<Diary> jawaban = diaryService.findWithUserId("Sedih", "Usedih");
		assertNotEquals(jawaban.size(), 0);
		diaryService.erase(1);
    }
	
	@Test
    public void whenFindWithUserIdIsCalledItShouldCallFindAll(){
		Diary diary = new Diary(1, "Usedih", "sedih", "Sedih");
        diaryService.register(diary);
        List<Diary> jawaban = diaryService.findWithUserId("Sedih", "Usedih");
		assertNotEquals(jawaban.size(), 0);
        jawaban = diaryService.findWithUserId("Semua", "Usedih");
		assertNotEquals(jawaban.size(), 0);
		diaryService.erase(1);
    }
	
	@Test
    public void isEmptyTest(){
		assertTrue(diaryService.isEmpty("Usedih", "Sedih"));
		Diary diary = new Diary(1, "Usedih", "sedih", "Sedih");
        diaryService.register(diary);
		assertFalse(diaryService.isEmpty("Usedih", "Sedih"));
		assertFalse(diaryService.isEmpty("Usedih", "Semua"));
		diaryService.erase(1);
    }
}
