package com.bot.buddybot.core.state;

import com.bot.buddybot.core.state.Command;
import com.bot.buddybot.core.state.ChooseActionState;
import com.bot.buddybot.core.state.ReadDiaryState;
import com.bot.buddybot.core.diary.core.Diary;
import com.bot.buddybot.core.diary.service.DiaryService;
import org.springframework.beans.factory.annotation.Autowired;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.bot.buddybot.core.mood.Kesepian;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.UserSource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.boot.test.context.SpringBootTest;
import java.time.Instant;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class TestReadDiaryState {
	
	@Autowired
	DiaryService diaryService;
	
	MessageEvent<TextMessageContent> messageEvent = new MessageEvent<>("replyToken", new UserSource("1"), new TextMessageContent("2","halo!"), Instant.parse("2020-01-01T00:00:00.000Z"));
	
    Command command;

    @BeforeEach
    public void setUp(){
		command = new Command();
		command.setState(new ReadDiaryState(command));
    }

    @Test
    public void testHelp(){
        String expectedOutput = "Perintah yang tersedia \n"
                    + "/help - "
					+ "Menunjukkan seluruh perintah yang tersedia \n"
					+ "/diarySemua - "
					+ "Menunjukkan seluruh diary \n"
					+ "/diaryStress - "
					+ "Melihat diary stress \n"
					+ "/diarySedih - "
					+ "Melihat diary sedih \n"
					+ "/diaryKesepian - "
					+ "Melihat diary kesepian \n"
					+ "/diaryMarah - "
					+ "Melihat diary marah \n"
					+ "/diaryMiskin - "
					+ "Melihat diary miskin \n"
					+ "/selesai - "
					+ "Selesai membaca diary \n"
					+ "/delete[id_diary] - "
					+ "Menghapus diary (menggunakan kurung siku) \n"
                    + "/mauNulis - "
					+ "Menulis diary";
		assertTrue(command.chooseDiaryCommand(messageEvent, diaryService, "/help").equals(expectedOutput));
    }
	
	@Test
	public void testChooseMoodCommand(){
		String expectedOutput = "Kamu belum kasih tau buddybot sedang merasakan apa";
		assertEquals(expectedOutput, command.chooseMoodCommand("test"));
	}
	
	@Test
	public void testChooseDiaryCommand(){
		String expectedOutput = "ketik /help untuk lihat command";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "/selesai"));
		
		command.setState(new ReadDiaryState(command));
		command.chooseActionState = new ChooseActionState(command, new Kesepian());
		expectedOutput = "Silahkan ketik apa yang ingin ditulis, "
				+"setelah selesai ketik /help untuk command lebih lanjut";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "/maunulis"));
		
		command.setState(new ReadDiaryState(command));
		expectedOutput = "Maaf, kamu belum menulis apa-apa";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "/diarysemua"));
		
		expectedOutput = "Maaf, kamu belum menulis apa-apa";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "/diarysedih"));
		
		expectedOutput = "Maaf, kamu belum menulis apa-apa";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "/diarystress"));
		
		expectedOutput = "Maaf, kamu belum menulis apa-apa";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "/diarykesepian"));
		
		expectedOutput = "Maaf, kamu belum menulis apa-apa";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "/diarymarah"));
		
		expectedOutput = "Maaf, kamu belum menulis apa-apa";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "/diarymiskin"));
		
		expectedOutput = "command tidak terdaftar";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "tes"));
		
		expectedOutput = "Delete gagal, coba cek apakah input yang dimasukkan benar";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "/delete[6]"));
		
		Diary diary = new Diary(1, "1", "sedih", "Sedih");
		diaryService.register(diary);
		String output = command.chooseDiaryCommand(messageEvent, diaryService, "/diarysedih");
		assertTrue(output.contains("diaryflag"));
		diaryService.erase(1);
	}
	
	@Test
	public void testGetQuickReplyHelp(){
		String output = command.getQuickReplyHelp();
		assertEquals("help diarySemua diaryStress diarySedih diaryKesepian diaryMarah diaryMiskin Selesai mauNulis", output);
	}
}