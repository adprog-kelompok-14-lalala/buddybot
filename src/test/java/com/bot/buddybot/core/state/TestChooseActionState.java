package com.bot.buddybot.core.state;

import com.bot.buddybot.core.state.Command;
import com.bot.buddybot.core.state.ChooseActionState;
import com.bot.buddybot.core.diary.service.DiaryService;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.bot.buddybot.core.mood.Kesepian;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import static org.junit.jupiter.api.Assertions.*;


public class TestChooseActionState {
	
	@Autowired
	DiaryService diaryService;
	
	@Autowired
	private MessageEvent<TextMessageContent> messageEvent;
	
    Command command;

    @BeforeEach
    public void setUp(){
		command = new Command();
		command.setState(new ChooseActionState(command, new Kesepian()));
    }

    @Test
    public void testHelp(){
        String expectedOutput = "Perintah yang tersedia \n" +
                    "/getFotoKucing - Foto-Foto kucing untuk kamu yang sedang kesepian \n" +
                    "/getTempatRefreshing - Link-Link video untuk kamu yang ingin ketawa \n" +
                    "/mauNulis - Menulis diary \n" +
					"/bacaDiary - Membaca diary \n" +
					"/gantiMood - Jika rasa kamu sudah berubah";
		assertEquals(command.chooseMoodCommand("/help"),expectedOutput);
    }
	
	@Test
	public void testChooseMood(){
		String jawaban = command.chooseMoodCommand("/getfotokucing");
		assertTrue(jawaban.contains("https"));
		
		jawaban = command.chooseMoodCommand("NoQuotes");
		assertTrue(jawaban.equals(""));
		
		jawaban = command.chooseMoodCommand("/gantimood");
		assertTrue(jawaban.equals("ketik /help untuk lihat command"));
		
		command.setState(new ChooseActionState(command, new Kesepian()));
		jawaban = command.chooseMoodCommand("tes");
		assertTrue(jawaban.equals("command tidak terdaftar"));
		
		jawaban = command.chooseMoodCommand("/bacadiary");
		String expectedOutput = "Baik, silahkan membaca diary, "
				+"ketik /help untuk perintah lebih lanjut";
		assertTrue(jawaban.equals(expectedOutput));
		
		command.setState(new ChooseActionState(command, new Kesepian()));
		jawaban = command.chooseMoodCommand("/maunulis");
		expectedOutput = "Silahkan ketik apa yang ingin ditulis, "
				+"setelah selesai ketik /selesai atau /bacaDiary. "
				+"Untuk informasi command lebih lanjut ketik /help";
		assertTrue(jawaban.equals(expectedOutput));
	}
	
	@Test
	public void testWriteDiary(){
		String expectedOutput = "Kamu belum kasih tau buddybot sedang merasakan apa";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "test"));
	}
	
	@Test
	public void testGetQuickReplyHelp(){
		String output = command.getQuickReplyHelp();
		assertEquals("help getFotoKucing getTempatRefreshing mauNulis bacaDiary gantiMood", output);
	}
}