package com.bot.buddybot.core.state;

import com.bot.buddybot.core.state.Command;
import com.bot.buddybot.core.state.ChooseActionState;
import com.bot.buddybot.core.state.WriteDiaryState;
import com.bot.buddybot.core.diary.service.DiaryService;
import org.springframework.beans.factory.annotation.Autowired;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.bot.buddybot.core.mood.Kesepian;
import com.linecorp.bot.model.event.MessageEvent;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.boot.test.context.SpringBootTest;
import com.linecorp.bot.model.event.source.UserSource;
import java.time.Instant;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class TestWriteDiaryState {
	
	@Autowired
	DiaryService diaryService;
	
	MessageEvent<TextMessageContent> messageEvent = new MessageEvent<>("replyToken", new UserSource("1"), new TextMessageContent("2","halo!"), Instant.parse("2020-01-01T00:00:00.000Z"));
	
    Command command;

    @BeforeEach
    public void setUp(){
		command = new Command();
		command.chooseActionState = new ChooseActionState(command, new Kesepian());
		command.setState(new WriteDiaryState(command, new Kesepian()));
    }

    @Test
    public void testHelp(){
        String expectedOutput = "Perintah yang tersedia \n"
                    + "/help - "
					+ "Menunjukkan seluruh perintah yang tersedia \n"
                    + "/selesai - "
					+ "Selesai menulis diary \n"
                    + "/bacaDiary - "
					+ "Menyimpan dan membaca diary";
		assertTrue(command.chooseDiaryCommand(messageEvent, diaryService,"/help").equals(expectedOutput));
    }
	
	@Test
	public void testCurrentStateIsDiaryMethod(){
		assertTrue(command.currentStateIsDiary());
	}
	
	@Test
	public void testChooseMoodCommand(){
		String expectedOutput = "Kamu belum kasih tau buddybot sedang merasakan apa";
		assertEquals(expectedOutput, command.chooseMoodCommand("test"));
	}
	
	@Test
	public void testChooseDiaryCommand(){
		String expectedOutput = "Diary telah tersimpan! "
            +"\n\nketik /help untuk lihat command";
		String messageId = messageEvent.getMessage().getId();
		String inputDelete = "/delete[" + messageId + "]";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "/selesai"));
		command.chooseMoodCommand("/bacadiary");
		command.chooseDiaryCommand(messageEvent, diaryService, inputDelete);
		
		command.setState(new WriteDiaryState(command, new Kesepian()));
		messageId = messageEvent.getMessage().getId();
		inputDelete = "/delete[" + messageId + "]";
		expectedOutput = "Diary telah tersimpan! "
            +"Untuk perintah lebih lanjut ketik /help";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "/bacadiary"));
		command.chooseDiaryCommand(messageEvent, diaryService, inputDelete);
		
		command.setState(new WriteDiaryState(command, new Kesepian()));
		expectedOutput = "";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "tes"));
	}
	
	@Test
	public void testGetQuickReplyHelp(){
		String output = command.getQuickReplyHelp();
		assertEquals("help Selesai bacaDiary", output);
	}
}
