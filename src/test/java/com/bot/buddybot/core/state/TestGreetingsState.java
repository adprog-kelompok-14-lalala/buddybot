package com.bot.buddybot.core.state;

import com.bot.buddybot.core.state.Command;
import com.bot.buddybot.core.state.GreetingsState;
import com.bot.buddybot.core.diary.service.DiaryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.jupiter.api.Assertions.*;


public class TestGreetingsState {
	
	@Autowired
	private DiaryService diaryService;
	
	@Autowired
	private MessageEvent<TextMessageContent> messageEvent;
	
    Command command;

    @BeforeEach
    public void setUp(){
		command = new Command();
		command.setState(new GreetingsState(command));
    }

    @Test
    public void testHelp(){
        String jawaban = "Perintah yang tersedia \n"
                    + "/help - Menunjukkan seluruh perintah yang tersedia \n"
                    + "/halo - Memulai percakapan dengan Buddy Bot (+corona update)";
		assertEquals(command.chooseMoodCommand("/help"),jawaban);
		assertEquals(command.help(),jawaban);
    }
	
	@Test
	public void testChooseMood(){
		String expectedOutput = "Bagaimana mood kamu hari ini? \n\n" + "untuk informasi command bisa ketik /help";
		assertEquals(expectedOutput, command.chooseMoodCommand("/halo"));
		
		command.setState(new GreetingsState(command));
		expectedOutput = "type /help if you want to see more of the bot's work";
		assertEquals(command.chooseMoodCommand("tes"), expectedOutput);
	}
	
	@Test
	public void testWriteDiary(){
		String expectedOutput = "Kamu belum kasih tau buddybot sedang merasakan apa";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "test"));
	}
	
	@Test
	public void testCurrentStateIsDiaryMethod(){
		assertFalse(command.currentStateIsDiary());
	}
	
	@Test
	public void testGetQuickReplyHelp(){
		String output = command.getQuickReplyHelp();
		assertEquals("help halo", output);
	}
}
