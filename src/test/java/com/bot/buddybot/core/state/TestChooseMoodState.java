package com.bot.buddybot.core.state;

import com.bot.buddybot.core.state.Command;
import com.bot.buddybot.core.state.ChooseMoodState;
import com.bot.buddybot.core.diary.service.DiaryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.jupiter.api.Assertions.*;


public class TestChooseMoodState {
	
	@Autowired
	DiaryService diaryService;
	
	@Autowired
	private MessageEvent<TextMessageContent> messageEvent;
	
    Command command;

    @BeforeEach
    public void setUp(){
		command = new Command();
		command.setState(new ChooseMoodState(command));
    }

    @Test
    public void testHelp(){
        String jawaban = "Perintah yang tersedia \n" +
                    "/help - Menunjukkan seluruh perintah yang tersedia \n" +
                    "/sedih - Melihat respon agar tidak sedih \n" +
                    "/stres - Melihat respon agar tidak stres \n" +
                    "/marah - Melihat respon agar tidak marah \n" +
                    "/kesepian - Melihat respon agar tidak kesepian \n" +
                    "/miskin - Melihat respon membantu agar tidak miskin\n";
		assertEquals(command.chooseMoodCommand("/help"),jawaban);
    }
	
	@Test
	public void testChooseMood(){
		String expectedOutput = "\n\nsilahkan ketik /help untuk melihat command baru yang tersedia";
		String jawaban = command.chooseMoodCommand("/sedih");
		assertTrue(jawaban.contains(expectedOutput));
		
		command.setState(new ChooseMoodState(command));
		jawaban = command.chooseMoodCommand("/marah");
		assertTrue(jawaban.contains(expectedOutput));
		
		command.setState(new ChooseMoodState(command));
		jawaban = command.chooseMoodCommand("/kesepian");
		assertTrue(jawaban.contains(expectedOutput));
		
		command.setState(new ChooseMoodState(command));
		jawaban = command.chooseMoodCommand("/miskin");
		assertTrue(jawaban.contains(expectedOutput));
		
		command.setState(new ChooseMoodState(command));
		jawaban = command.chooseMoodCommand("/stres");
		assertTrue(jawaban.contains(expectedOutput));
		
		command.setState(new ChooseMoodState(command));
		jawaban = command.chooseMoodCommand("tes");
		assertTrue(jawaban.equals("command tidak terdaftar"));
	}
	
	@Test
	public void testWriteDiary(){
		String expectedOutput = "Kamu belum kasih tau buddybot sedang merasakan apa";
		assertEquals(expectedOutput, command.chooseDiaryCommand(messageEvent, diaryService, "test"));
	}
	
	@Test
	public void testGetQuickReplyHelp(){
		String output = command.getQuickReplyHelp();
		assertEquals("help Sedih Stres Marah Kesepian Miskin", output);
	}
}