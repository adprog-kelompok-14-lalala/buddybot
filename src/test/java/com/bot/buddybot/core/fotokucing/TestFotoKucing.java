package com.bot.buddybot.core.fotokucing;

import com.bot.buddybot.core.fotokucing.IFotoKucing;
import com.bot.buddybot.core.fotokucing.FotoKucing;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;


public class TestFotoKucing {
    IFotoKucing testFoto;
    ArrayList<String> kumpulanfotokucing;

    @BeforeEach
    public void setUp(){
        testFoto = new FotoKucing();
    }

    @Test
    public void testFoto(){
        IFotoKucing testFoto = new FotoKucing();
        assertNotNull(testFoto);
    }

    @Test
    public void testGetFoto(){
        String foto = testFoto.getFotoKucing();
        assertEquals("java.lang.String", foto.getClass().getName());
        String[] fotoList = foto.substring(10).split(",");
        assertTrue(fotoList[0].contains("http"));
        assertTrue(fotoList[1].contains("http"));
        assertTrue(fotoList[2].contains("http"));
        assertTrue(fotoList[3].contains("http"));
        assertTrue(fotoList[4].contains("http"));
    }
}
