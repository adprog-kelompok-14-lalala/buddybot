package com.bot.buddybot.core.fotokucing;

import com.bot.buddybot.core.fotokucing.IFotoKucing;
import com.bot.buddybot.core.fotokucing.NoFoto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestNoFoto {
    IFotoKucing testNoFoto;

    @BeforeEach
    public void setUp(){
        testNoFoto = new NoFoto();
    }


    @Test
    public void testNoFotoExists(){
        IFotoKucing testNoFoto = new NoFoto();

        assertNotNull(testNoFoto);
    }

    @Test
    public void testGetFoto(){
        assertNull(testNoFoto.getFotoKucing());
    }
}
