package com.bot.buddybot.core.fotokucing;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FotoKucing implements IFotoKucing {

    public FotoKucing(){
		//EMPTY
	}

    public String getFotoKucing(){
		JSONArray jsonArray = getFotoKucingJson();
		return getStringOfFotoKucing(jsonArray);
    }
	
	public JSONArray getFotoKucingJson(){
		try{
			int index = new Random().nextInt(100);
			HttpResponse<JsonNode> jsonResponse = Unirest.get("https://api.qwant.com/api/search/images").queryString("q","cute cats")
					.queryString("count","5")
					.queryString("offset",Integer.toString(index))
					.queryString("t", "images")
					.queryString("safesearch","1")
					.queryString("locale","en_US")
					.queryString("uiv", "4")
					.asJson();
			return jsonResponse.getBody().getObject().getJSONObject("data")
					.getJSONObject("result").getJSONArray("items");
		}catch(JSONException | UnirestException e){
			e.printStackTrace();
			return new JSONArray();
		}
	}
	
	public String getStringOfFotoKucing(JSONArray jsonArray){
		try{
			String output = "kucingflag";
			for(int i = 0;i<jsonArray.length();i++){
				JSONObject jsonObj = jsonArray.getJSONObject(i);
				output += jsonObj.getString("media").replace("http://","https://") + ",";
			}
			output = output.substring(0,output.length()-1);
			return handleFullTraffic(output);
		}catch(JSONException e){
			e.printStackTrace();
			return "maaf, kucingnya sedang tidak mau diliat";
		}
	}
	
	private String handleFullTraffic(String output){
		if(output.equals("kucingfla")){
			return "maaf, kucingnya sedang tidak mau diliat";
		}else{
			return output;
		}
	}
}
