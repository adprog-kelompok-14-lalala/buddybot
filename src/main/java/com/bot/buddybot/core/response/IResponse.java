package com.bot.buddybot.core.response;

public interface IResponse {
    public String getResponse();
}
