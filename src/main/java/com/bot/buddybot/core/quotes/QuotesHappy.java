package com.bot.buddybot.core.quotes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class QuotesHappy implements IQuote {
    ArrayList<String> kumpulanQuotesHappy = new ArrayList<>(Arrays.asList(
            "\"Nothing always stay the same. "
                    +"You don't stay happy forever. "
                    +"You don't stay sad forever.\" -Cat Zingano",
            "\"It will get better. It maybe stormy now but it can't rain forever.\"",
            "\"A certain darkness is needed to see the stars.\"",
            "\"It's okay to be sad. It's okay to cry. Just don't let the sadness"
                    +"take over your life. You will be smiling soon.\"",
            "\"Someday, everything will make perfect sense. Everything happens for a reason.\"",
            "\"Happiness comes in waves. It'll find you again.\"",
            "\"Some days are just bad days, that's all. You have to experience "
                    + "sadness to know happiness. Remind yourself, not every day "
                    + "is going to be a good day. That's just the way it is!\""
    ));

    public QuotesHappy(){
		//EMPTY
	}
	
    public String getQuotes() {
        Random random = new Random();
        int randomNumber = random.nextInt(kumpulanQuotesHappy.size());
        return kumpulanQuotesHappy.get(randomNumber);
    }
}
