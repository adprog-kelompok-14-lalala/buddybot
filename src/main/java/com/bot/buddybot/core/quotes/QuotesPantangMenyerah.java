package com.bot.buddybot.core.quotes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class QuotesPantangMenyerah implements IQuote {
    ArrayList<String> kumpulanQuotesPantangMenyerah = new ArrayList<>(Arrays.asList(
            "\"Just because we're in a stressful situation, "
                    +"doesn't mean that we have to get stressed out. "
                    +"You may be in the storm. The key is, "
                    +"don't let the storm get in you.\" -Joel Oesteen",
            "\"Don't be stressed over what we can't control.\"",
            "\"Breathe in, breathe out.\"",
            "\"Don't stress, do your best, forget the rest.\"",
            "\"Stressed spelled backwards is desserts.\" -Loretta Laroche",
            "\"Your calm mind is the ultimate weapon against your challenges. "
                    + "So relax.\" -Bryant McGill",
            "\"You can do this!\""
    ));

    public QuotesPantangMenyerah(){
        //EMPTY
	}
	
    public String getQuotes() {
        Random random = new Random();
        int randomNumber = random.nextInt(kumpulanQuotesPantangMenyerah.size());
        return kumpulanQuotesPantangMenyerah.get(randomNumber);
    }
}
