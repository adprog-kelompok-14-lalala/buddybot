package com.bot.buddybot.core.quotes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class QuotesSabar implements IQuote{
	ArrayList<String> kumpulanquotessabar = new ArrayList<>(Arrays.asList( "Yang sabar yaa. "
			+"Sesungguhnya setalah kesulitan ada kemudahan.",
            "Untuk mencintai, kamu harus memiliki kekuatan. "
			+"Kekuatan melawan amarah dengan kesabaran "
			+"dan kekuatan memaafkan dengan ketulusan. – Mario Teguh",
            "“Ternyata, banyak hal yang tidak selesai dengan amarah.” – Iwan Fals",
            "“Cinta dan kelembutan adalah sifat manusia, "
			+"amarah dan gairah nafsu adalah sifat binatang.” – Jalaluddin Rumi",
            "“Saat kau membalas kebencian dengan amarah dan caci maki, "
			+"saat itulah musuhmu menang.” – Andy F. Noya",
            "“Siapakah orang yang kurang ilmu? "
			+"dialah orang yang mengandalkan otot "
			+"dan amarah dalam menyikapi segala sesuatu.” – Abdullah Gymnastiar",
            "“Ketika Anda marah, hitunglah sampai sepuluh sebelum Anda berbicara. "
			+"Jika Anda sangat marah, hitunglah sampai seratus.” – Thomas Jefferson",
            " “Siapapun bisa marah, karena marah itu mudah. "
			+"Tetapi marah kepada siapa, dengan kadar kemarahan yang pas, "
			+"pada saat dan tujuan yang tepat, "
			+"serta dengan cara benar itu yang sulit.” – Aristoteles",
            "“Lemparkan senyum kepada yang memancing amarah.” – Harry Slyman"
    ));
	
    public QuotesSabar(){
		//EMPTY
	}
	
    public String getQuotes() {
        Random rng = new Random();
        int randNumber = rng.nextInt(kumpulanquotessabar.size());
        return kumpulanquotessabar.get(randNumber);
    }
}
