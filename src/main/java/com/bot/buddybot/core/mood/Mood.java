package com.bot.buddybot.core.mood;

import com.bot.buddybot.core.fotokucing.IFotoKucing;
import com.bot.buddybot.core.quotes.IQuote;
import com.bot.buddybot.core.response.IResponse;
import com.bot.buddybot.core.saran.ISaran;

public abstract class Mood {
    protected IQuote quote;
    protected ISaran saran;
    protected IFotoKucing foto;
    protected IResponse response;
    public String[] command;

    public Mood(){
	}

    public String getQuotes(){
        return quote.getQuotes();
    }

    public String getSaran(){
        return saran.getSaran();
    }

    public String getFotoKucing(){
        return foto.getFotoKucing();
    }

    public String getResponses(){
        return response.getResponse();
    }

    public void setQuote(IQuote quote){
        this.quote = quote;
    }

    public void setSaran(ISaran saran){
        this.saran = saran;
    }

    public void setFoto(IFotoKucing foto) {
        this.foto = foto;
    }

    public void setResponse(IResponse response) {
        this.response = response;
    }

    public abstract String getHelp();
	
	public abstract String getQuickReplyHelp();
}
