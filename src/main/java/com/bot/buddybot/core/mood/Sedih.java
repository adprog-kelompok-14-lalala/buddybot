package com.bot.buddybot.core.mood;

import com.bot.buddybot.core.fotokucing.NoFoto;
import com.bot.buddybot.core.quotes.QuotesHappy;
import com.bot.buddybot.core.response.ResponseSedih;
import com.bot.buddybot.core.saran.SaranVideoLucu;

public class Sedih extends Mood{

    public Sedih(){
        super();
        command = new String[]{"/gethappyquotes",
							   "/getsaranvideolucu",
							   "NoFoto",
							   "/maunulis",
							   "/bacadiary"};
        quote = new QuotesHappy();
        saran = new SaranVideoLucu();
        response = new ResponseSedih();
        foto = new NoFoto();
    }

    public String getHelp(){
        String help = "Perintah yang tersedia \n"
                + "/getHappyQuotes - Quotes-Quotes happy untuk kamu yang lagi sedih \n"
                + "/getSaranVideoLucu - Link-Link video untuk kamu yang ingin ketawa \n"
                + "/mauNulis - Menulis diary \n"
                + "/bacaDiary - Membaca diary \n"
                + "/gantiMood - Jika rasa kamu sudah berubah";
        return help;
    }
	
	public String getQuickReplyHelp(){
		String quickReplyHelp = "help getHappyQuotes getSaranVideoLucu "
                                +"mauNulis bacaDiary gantiMood";
		return quickReplyHelp;
	}
}
