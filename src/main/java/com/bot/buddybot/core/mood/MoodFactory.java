package com.bot.buddybot.core.mood;

public class MoodFactory {
    private MoodFactory(){
    }

    public static Mood createMood(String type){
        Mood mood;
        if (type.equals("/sedih")){
            mood = new Sedih();
        }else if (type.equals("/stres")){
            mood = new Stress();
        }else if (type.equals("/miskin")){
            mood = new Miskin();
        }else if (type.equals("/marah")){
            mood = new Marah();
        }else{
            mood = new Kesepian();
        }
        return mood;
    }
}
