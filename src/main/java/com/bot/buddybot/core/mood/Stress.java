package com.bot.buddybot.core.mood;

import com.bot.buddybot.core.fotokucing.NoFoto;
import com.bot.buddybot.core.quotes.QuotesPantangMenyerah;
import com.bot.buddybot.core.response.ResponseStress;
import com.bot.buddybot.core.saran.SaranTempatRefreshing;

public class Stress extends Mood {

    public Stress(){
        super();
        command = new String[]{"/getquotestetapkuat",
							   "/gettempatrefreshing",
							   "NoFoto",
							   "/maunulis",
							   "/bacadiary"};
        quote = new QuotesPantangMenyerah();
        saran = new SaranTempatRefreshing();
        response = new ResponseStress();
        foto = new NoFoto();
    }

    public String getHelp(){
        String help = "Perintah yang tersedia \n"
                + "/getQuotesTetapKuat - "
				+ "Quotes-Quotes untuk kamu yang ingin menyerah \n"
                + "/getTempatRefreshing - "
				+ "Saran-Saran tempat untuk kamu yang ingin refreshing \n"
                + "/mauNulis - Menulis diary \n"
                + "/bacaDiary - Membaca diary \n"
                + "/gantiMood - Jika rasa kamu sudah berubah";
        return help;
    }
	
	public String getQuickReplyHelp(){
		String quickReplyHelp = "help getQuotesTetapKuat getTempatRefreshing "
                                +"mauNulis bacaDiary gantiMood";
		return quickReplyHelp;
	}
}
