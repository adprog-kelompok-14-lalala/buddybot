package com.bot.buddybot.core.mood;

import com.bot.buddybot.core.fotokucing.NoFoto;
import com.bot.buddybot.core.quotes.NoQuotes;
import com.bot.buddybot.core.response.ResponseSemangatCariDuit;
import com.bot.buddybot.core.saran.SaranMagang;

public class Miskin extends Mood {

    public Miskin(){
        super();
        command = new String[]{"NoQuotes",
							   "/getsaranmagang",
							   "NoFoto",
							   "/maunulis",
							   "/bacadiary"};
        quote = new NoQuotes();
        saran = new SaranMagang();
        response = new ResponseSemangatCariDuit();
        foto = new NoFoto();
    }

    public String getHelp(){
        String help = "Perintah yang tersedia \n"
                + "/getSaranMagang - Link-Link magang untuk kamu yang ingin punya duit \n"
                + "/mauNulis - Menulis diary \n"
                + "/bacaDiary - Membaca diary \n"
                + "/gantiMood - Jika rasa kamu sudah berubah";
        return help;
    }
	
	public String getQuickReplyHelp(){
		String quickReplyHelp = "help getSaranMagang mauNulis bacaDiary gantiMood";
		return quickReplyHelp;
	}
}
