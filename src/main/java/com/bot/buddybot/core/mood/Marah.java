package com.bot.buddybot.core.mood;

import com.bot.buddybot.core.fotokucing.NoFoto;
import com.bot.buddybot.core.quotes.QuotesSabar;
import com.bot.buddybot.core.response.ResponseSabarAja;
import com.bot.buddybot.core.saran.NoSaran;

public class Marah extends Mood {

    public Marah(){
        super();
        command = new String[]{"/getquotessabar",
								"NoSaran",
								"NoFoto",
								"/maunulis",
								"/bacadiary"};
        quote = new QuotesSabar();
        saran = new NoSaran();
        response = new ResponseSabarAja();
        foto = new NoFoto();
    }

    public String getHelp(){
        String help = "Perintah yang tersedia \n"
                + "/getQuotesSabar - "
				+ "Quotes-Quotes bersabar untuk kamu yang sedang emosi \n"
                + "/mauNulis - Menulis diary \n"
                + "/bacaDiary - Membaca diary \n"
                + "/gantiMood - Jika rasa kamu sudah berubah";
        return help;
    }
	
	public String getQuickReplyHelp(){
		String quickReplyHelp = "help getQuotesSabar mauNulis bacaDiary gantiMood";
		return quickReplyHelp;
	}
}
