package com.bot.buddybot.core.mood;

import com.bot.buddybot.core.fotokucing.FotoKucing;
import com.bot.buddybot.core.quotes.NoQuotes;
import com.bot.buddybot.core.response.ResponseTidurAja;
import com.bot.buddybot.core.saran.SaranTempatRefreshing;

public class Kesepian extends Mood {
	 
    public Kesepian(){
        super();
		command = new String[]{"NoQuotes",
				"/gettempatrefreshing",
				"/getfotokucing",
				"/maunulis",
				"/bacadiary"};
        quote = new NoQuotes();
        saran = new SaranTempatRefreshing();
        response = new ResponseTidurAja();
        foto = new FotoKucing();
    }
	
	//return command available in String form
	public String getHelp(){
		String help = "Perintah yang tersedia \n"
                    + "/getFotoKucing - Foto-Foto kucing untuk kamu yang sedang kesepian \n"
                    + "/getTempatRefreshing - Link-Link video untuk kamu yang ingin ketawa \n"
                    + "/mauNulis - Menulis diary \n"
					+ "/bacaDiary - Membaca diary \n"
					+ "/gantiMood - Jika rasa kamu sudah berubah";
		return help;
	}
	
	public String getQuickReplyHelp(){
		String quickReplyHelp = "help getFotoKucing getTempatRefreshing "
								+"mauNulis bacaDiary gantiMood";
		return quickReplyHelp;
	}
}
