package com.bot.buddybot.core.saran;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class SaranVideoLucu implements ISaran {
	ArrayList<String> kumpulanlink = new ArrayList<>(Arrays.asList(
            "https://www.youtube.com/watch?v=PI4JABNwB_U",
            "https://www.youtube.com/watch?v=5jpkxyaA_YI",
            "https://www.youtube.com/watch?v=5DzTwbyBhEc",
            "https://www.youtube.com/watch?v=VWWUP1vzcuU",
            "https://www.youtube.com/watch?v=mxn9Zdlqv_A",
            "https://www.youtube.com/watch?v=SaS4hjbJa1U",
            "https://www.youtube.com/watch?v=JHy6bBKu0j4"
    ));
	
    public SaranVideoLucu(){
		//EMPTY
	}

    public String getSaran() {
        String video;
        int index = new Random().nextInt(kumpulanlink.size());
        video = kumpulanlink.get(index);

        return "Nonton video lucu ajaa, biar hepi! \nKayak video ini nihh: "
                + video;
    }
}
