package com.bot.buddybot.core.saran;

import java.util.ArrayList;
import java.util.Arrays;

public class SaranTempatRefreshing implements ISaran {
    ArrayList<String> kumpulanSaran = new ArrayList<>(Arrays.asList(
           "https://i1.wp.com/www.jajanbeken.com/wp-content/uploads/2018/12/jajanbeken-bogor-botanical-garden-entrance.jpg?fit=1199%2C673;"
                   + "Kebun Raya Bogor;Bogor",
           "https://media.suara.com/pictures/original/2019/12/07/90846-jerapa.jpg;Taman Safari Indonesia;Cisarua",
           "https://upload.wikimedia.org/wikipedia/commons/7/78/Ragunan_Zoo_Gate.jpg;Kebun Binatang Ragunan;Pasar Minggu",
           "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS8lzE1CXiE8dxgwUxfVRdnrpDsxpNGHkJiAIIisXhIbR7u8Alp&usqp=CAU;"
                   + "Perpustakaan Nasional;Jakarta Pusat",
           "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSZytoEP7LBedLgT00tQYaGdwIvC0gcPsDiPcMIJDf6gOUx16fn&usqp=CAU;"
                   + "Cimory Riverside;Cisarua",
           "https://anekatempatwisata.com/wp-content/uploads/2014/08/Kawah-Putih-Bandung.jpg;Kawah Putih;Bandung"
    ));

    public SaranTempatRefreshing(){
        //EMPTY
    }

    public String getSaran() {
        String strLink = "refreshflag" + String.join(",", kumpulanSaran);
        return strLink;
    }
}
