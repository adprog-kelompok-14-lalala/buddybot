package com.bot.buddybot.core.saran;

import java.util.ArrayList;

public class SaranMagang implements ISaran {
	ArrayList<String> kumpulanMagang;

    public SaranMagang(){
		//EMPTY
	}

    public String getSaran() {
        return "Lagi butuh duit ya?:( Tinggal dicari aja kak! \nWebsite-website lowongan magang:"
                +"\n- https://www.kalibrr.id/"
                +"\n- https://magang.id/"
                +"\n- https://magang.co.id"
                +"\n- https://www.loker.id/tipe-pekerjaan/magang"
                +"\n- https://www.jobstreet.co.id/en/job-search/magang-jobs/"
                +"\n- http://studentjob.co.id//"
                +"\n- https://www.topkarir.com/lowongan/magang"
                +"\n- https://www.careerjet.co.id/"
                +"\n SEMANGAT <3";
    }
}
