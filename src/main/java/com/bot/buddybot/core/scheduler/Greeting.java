package com.bot.buddybot.core.scheduler;

import com.bot.buddybot.controller.BuddyBotController;
import java.util.ArrayList;
import java.util.Arrays;

public class Greeting extends BroadcastChat implements Runnable {

    public Greeting(BuddyBotController controller){
        super(controller);
        this.kumpulanChat = new ArrayList<>(Arrays.asList(
                "SELAMAT PAGI TEMAN BUDDY BOT! SEMANGAT MENJALANI HARI INI YA",
                "SELAMAT PAGI! AWALI HARI INI DENGAN SENYUMAN",
                "SELAMAT PAGI, SEMOGA PAGI YANG CERAH INI AKAN MENGANTARKAN "
                        + "HARI-HARIMU MENJADI LEBIH INDAH",
                "PAGI MERUPAKAN JALAN UNTUK MELIHAT DUNIA BARU, AYO BANGUN!",
                "SAMBUT PAGI DENGAN SENYUMAN DAN SEMANGAT. SELAMAT PAGI!"
        ));

    }

    @Override
    public void run() {
        try {
            String todaysGreeting = getRandomChat();
            this.controller.broadcastReply(todaysGreeting);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}