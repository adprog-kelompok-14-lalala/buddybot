package com.bot.buddybot.core.scheduler;

import com.bot.buddybot.controller.BuddyBotController;
import java.util.ArrayList;
import java.util.Arrays;

public class Daily extends BroadcastChat implements Runnable {

    public Daily(BuddyBotController controller){
        super(controller);
        this.kumpulanChat = new ArrayList<>(Arrays.asList(
                "Berhenti Menyalahkan Segalanya",
                "Ambil Risiko, Bermimpi Lebih Besar, dan Berharap Besar",
                "Kerjakan dengan Lebih dan Sepenuh Hati",
                "Lakukan Apa yang Membuatmu Bahagia",
                "Jangan Pernah Menyerah Apapun yang Terjadi",
                "Syukuri dan Hargai  Hal-Hal yang Anda Miliki",
                "Nikmati dan Hargai Perubahan dalam Kehidupan",
                "Keraguan adalah Musuh Terbesar dalam Meraih Mimpi",
                "Memaafkan Membuat Anda Menjadi Pribadi yang Semakin Kuat",
                "Selalu Lakukan Perubahan Kecil ke Arah yang Lebih Baik",
                "Berhentilah Hanya Ketika Anda Berhasil Mengalahkan Tantangan"
        ));
    }

    @Override
    public void run() {
        try {
            String todaysQuote = getRandomChat();
            this.controller.broadcastReply("Daily quotes! \n"
                    +"\n" + todaysQuote);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}