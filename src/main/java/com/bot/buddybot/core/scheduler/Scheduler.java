package com.bot.buddybot.core.scheduler;

import com.bot.buddybot.controller.BuddyBotController;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Scheduler {
    BuddyBotController controller;

    public Scheduler(BuddyBotController controller){
        this.controller = controller;
        this.makeScheduleDaily();

        LocalDateTime now = LocalDateTime.now().plus(7, ChronoUnit.HOURS);
        String dayNow = now.getDayOfWeek().name();
        if(dayNow.equals("MONDAY")
                || dayNow.equals("SATURDAY")){
            this.makeScheduleGreeting();
        }
    }

    public void makeScheduleDaily(){
        LocalTime waktu = LocalTime.of(17, 0);
        LocalTime now = LocalTime.now().plus(7,ChronoUnit.HOURS);

        long delay = ChronoUnit.SECONDS.between(now,waktu);

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(
                new Daily(this.controller), delay, 86400, TimeUnit.SECONDS);
    }

    public void makeScheduleGreeting(){
        LocalTime waktu = LocalTime.of(8, 0);
        LocalTime now = LocalTime.now().plus(7,ChronoUnit.HOURS);

        long delay = ChronoUnit.SECONDS.between(now,waktu);

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(
                new Greeting(this.controller), delay, 604800, TimeUnit.SECONDS);
    }
}
