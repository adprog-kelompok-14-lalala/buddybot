package com.bot.buddybot.core.scheduler;

import com.bot.buddybot.controller.BuddyBotController;
import java.util.ArrayList;
import java.util.Random;

public abstract class BroadcastChat {
    protected BuddyBotController controller;
    protected ArrayList<String> kumpulanChat;

    public BroadcastChat(BuddyBotController controller){
        this.controller = controller;
    }

    public String getRandomChat(){
        Random rng = new Random();
        int randNumber = rng.nextInt(kumpulanChat.size());
        String todaysChat = kumpulanChat.get(randNumber);
        return todaysChat;
    }
}
