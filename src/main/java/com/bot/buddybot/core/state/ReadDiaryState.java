package com.bot.buddybot.core.state;

import com.bot.buddybot.core.diary.service.DiaryService;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import java.util.Arrays;
import java.util.List;

public class ReadDiaryState implements CommandState{

    Command command;

    public ReadDiaryState(Command command){
        this.command = command;
    }
	
	@Override
	public String help(){
		String jawaban = "Perintah yang tersedia \n"
                    + "/help - "
					+ "Menunjukkan seluruh perintah yang tersedia \n"
					+ "/diarySemua - "
					+ "Menunjukkan seluruh diary \n"
					+ "/diaryStress - "
					+ "Melihat diary stress \n"
					+ "/diarySedih - "
					+ "Melihat diary sedih \n"
					+ "/diaryKesepian - "
					+ "Melihat diary kesepian \n"
					+ "/diaryMarah - "
					+ "Melihat diary marah \n"
					+ "/diaryMiskin - "
					+ "Melihat diary miskin \n"
                    + "/selesai - "
					+ "Selesai membaca diary \n"
					+ "/delete[id_diary] - "
					+ "Menghapus diary (menggunakan kurung siku) \n"
                    + "/mauNulis - "
					+ "Menulis diary";
		return jawaban;
	}
	
	@Override
	public String getQuickReplyHelp(){
		String quickReplyHelp = "help diarySemua "
								+"diaryStress diarySedih "
								+"diaryKesepian diaryMarah "
								+"diaryMiskin Selesai mauNulis";
		return quickReplyHelp;
	}

    @Override
    public String chooseMoodCommand(String pesan) {
		return "Kamu belum kasih tau buddybot sedang merasakan apa";
    }
	
	@Override
	public String chooseDiaryCommand(MessageEvent<TextMessageContent> messageEvent,
									  DiaryService diaryService,
									  String pesan) {
		String jawaban = "command tidak terdaftar";
		String[] getDiaryInput = new String[] {"/diarystress", 
							"/diarysedih", 
							"/diarykesepian", 
							"/diarymarah", 
							"/diarymiskin", 
							"/diarysemua"};
		String[] stateChangeInput = new String[] {"/selesai", "/maunulis"};
		List<String> stateChangeInputList = Arrays.asList(stateChangeInput);
		List<String> getDiaryInputList = Arrays.asList(getDiaryInput);
		String userId = messageEvent.getSource().getUserId();
		if(pesan.equals("/help")){
			return help();
		}else if(getDiaryInputList.contains(pesan) || pesan.contains("/diaryview")){
			jawaban = handleDiaryInput(diaryService, pesan, userId);
		}else if(stateChangeInputList.contains(pesan)){
            jawaban = handleChangeStateInputs(pesan);
        }else if(pesan.contains("/delete")) {
			jawaban = deleteDiaryByMessageId(diaryService, pesan);
        }return jawaban;
    }
	
	private String deleteDiaryByMessageId(DiaryService diaryService, String pesan){
		try{
			String messageId = pesan.substring(8,pesan.length()-1);
			diaryService.erase(Long.parseLong(messageId));
			return "Diary berhasil dihapus";
		}catch(Exception e){
			return "Delete gagal, coba cek apakah input yang dimasukkan benar";
		}
	}

	private void setStateToWrite(){
		command.setState(command.chooseActionState);
		command.chooseMoodCommand("/maunulis");
	}
	
	private String handleDiaryInput(DiaryService diaryService, String pesan, String userId){
		String jawaban = "diaryflag";
		boolean isEmpty = false;
		String moodInput = pesan.substring(6);
		String moodClass = moodInput.substring(0,1).toUpperCase();
		moodClass += moodInput.substring(1);		
		jawaban += userId + ";" + moodClass;
		if(diaryService.isEmpty(userId, moodClass) && !pesan.contains("/diaryview")){
			return "Maaf, kamu belum menulis apa-apa";
		}return jawaban;
	}
	
	private String handleChangeStateInputs(String pesan){
		String jawaban;
		if(pesan.equals("/selesai")){
            command.setState(this.command.chooseActionState);
			jawaban = "ketik /help untuk lihat command";
        }else{
            setStateToWrite();
			jawaban = "Silahkan ketik apa yang ingin ditulis, "
					+"setelah selesai ketik /help untuk command lebih lanjut";
        }
		return jawaban;
	}
}
