package com.bot.buddybot.core.state;

import com.bot.buddybot.core.diary.service.DiaryService;
import com.bot.buddybot.core.mood.Mood;
import com.bot.buddybot.core.mood.MoodFactory;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import java.util.Arrays;
import java.util.List;

public class ChooseMoodState implements CommandState{

    Command command;
	Mood mood;

    public ChooseMoodState(Command command){
        this.command = command;
    }
	
	//return informations of available commands in String form
	@Override
	public String help(){
		String jawaban = "Perintah yang tersedia \n"
                    + "/help - Menunjukkan seluruh perintah yang tersedia \n"
                    + "/sedih - Melihat respon agar tidak sedih \n"
                    + "/stres - Melihat respon agar tidak stres \n"
                    + "/marah - Melihat respon agar tidak marah \n"
                    + "/kesepian - Melihat respon agar tidak kesepian \n"
                    + "/miskin - Melihat respon membantu agar tidak miskin\n";
		return jawaban;
	}
	
	@Override
	public String getQuickReplyHelp(){
		String quickReplyHelp = "help Sedih Stres Marah Kesepian Miskin";
		return quickReplyHelp;
	}
	
	//handle input given by user
    @Override
    public String chooseMoodCommand(String pesan){
		String jawaban;
		String[] chooseMoodInput = new String[] {"/sedih", 
									"/stres", 
									"/marah", 
									"/kesepian", 
									"/miskin"};
		List<String> chooseMoodInputList = Arrays.asList(chooseMoodInput);
		if(chooseMoodInputList.contains(pesan)){
			mood = MoodFactory.createMood(pesan);
			jawaban = mood.getResponses();
		}
		else if(pesan.equals("/help")){
			return help();
		}
        else{
            return "command tidak terdaftar";
        }
		jawaban += "\n\nsilahkan ketik /help untuk melihat command baru yang tersedia";
		setCommandActionState(command, mood);
		return jawaban;
    }

	@Override
	public String chooseDiaryCommand(MessageEvent<TextMessageContent> messageEvent,
									  DiaryService diaryService,
									  String pesan) {
         return "Kamu belum kasih tau buddybot sedang merasakan apa";
    }

	private void setCommandActionState(Command command, Mood mood){
		command.chooseActionState = new ChooseActionState(command,mood);
		command.setState(command.chooseActionState);
	}
}