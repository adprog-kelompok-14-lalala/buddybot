package com.bot.buddybot.core.state;

import com.bot.buddybot.core.diary.service.DiaryService;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;

public class GreetingsState implements CommandState{

    Command command;

    public GreetingsState(Command command){
        this.command = command;
    }
	
	//return informations of available commands in String form
	@Override
	public String help(){
		String jawaban = "Perintah yang tersedia \n"
                    + "/help - Menunjukkan seluruh perintah yang tersedia \n"
                    + "/halo - Memulai percakapan dengan Buddy Bot (+corona update)";
		return jawaban;
	}
	
	@Override
	public String getQuickReplyHelp(){
		String quickReplyHelp = "help halo";
		return quickReplyHelp;
	}
	
	//handle input given by user
    @Override
    public String chooseMoodCommand(String pesan){
		String jawaban = "";
		if(pesan.equals("/halo")){
            jawaban = "Bagaimana mood kamu hari ini? \n\n"
					+ "untuk informasi command "
					+ "bisa ketik /help";
			command.setState(command.chooseMoodState);
        }else if(pesan.equals("/help")){
			jawaban = help();
		}else{
			jawaban="type /help if you want to see more of the bot's work";            
        }
		return jawaban;
    }
	
	@Override
	public String chooseDiaryCommand(MessageEvent<TextMessageContent> messageEvent,
									  DiaryService diaryService,
									  String pesan) {
        return "Kamu belum kasih tau buddybot sedang merasakan apa";
    }
}
