package com.bot.buddybot.core.state;

import com.bot.buddybot.core.diary.core.Diary;
import com.bot.buddybot.core.diary.service.DiaryService;
import com.bot.buddybot.core.mood.Mood;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;

public class WriteDiaryState implements CommandState{

    Command command;
	Mood mood;
	String isiDiary = "";

    public WriteDiaryState(Command command, Mood mood){
        this.command = command;
		this.mood = mood;
    }
	
	@Override
	public String help(){
		String jawaban = "Perintah yang tersedia \n"
                    + "/help - "
					+ "Menunjukkan seluruh perintah yang tersedia \n"
                    + "/selesai - "
					+ "Selesai menulis diary \n"
                    + "/bacaDiary - "
					+ "Menyimpan dan membaca diary";
		return jawaban;
	}
	
	@Override
	public String getQuickReplyHelp(){
		String quickReplyHelp = "help Selesai bacaDiary";
		return quickReplyHelp;
	}

    @Override
    public String chooseMoodCommand(String pesan) {
		return "Kamu belum kasih tau buddybot sedang merasakan apa";
    }
	
	@Override
	public String chooseDiaryCommand(MessageEvent<TextMessageContent> messageEvent,
									  DiaryService diaryService,
									  String pesan){
		String jawaban;
		if(pesan.equals("/help")){
			jawaban = help();
		}else if(pesan.equals("/selesai")){
			createDiary(messageEvent, diaryService, isiDiary);
            command.setState(this.command.chooseActionState);
			jawaban = "Diary telah tersimpan! "
					+"\n\nketik /help untuk lihat command";
        }else if(pesan.equals("/bacadiary")){
			createDiary(messageEvent, diaryService, isiDiary);
            command.setState(this.command.readDiaryState);
			jawaban = "Diary telah tersimpan! "
					+"Untuk perintah lebih lanjut ketik /help";
        }else{
			isiDiary += pesan + ". ";
			return "";
		}
		return jawaban;
    }
	
	private void createDiary(MessageEvent<TextMessageContent> messageEvent,
							 DiaryService diaryService,
							 String pesan){
		long messageId = Long.parseLong(messageEvent.getMessage().getId());
		String userId = messageEvent.getSource().getUserId();
		Diary diary = new Diary(messageId, userId, pesan, mood.getClass().getSimpleName());
		diaryService.register(diary);
	}
}