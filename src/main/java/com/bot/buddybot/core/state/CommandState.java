package com.bot.buddybot.core.state;

import com.bot.buddybot.core.diary.service.DiaryService;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;

public interface CommandState {
    public String chooseMoodCommand(String pesan);
	
    public String chooseDiaryCommand(MessageEvent<TextMessageContent> messageEvent,
									  DiaryService diaryService,
									  String pesan);
	
	public String help();
	
	public String getQuickReplyHelp();
}