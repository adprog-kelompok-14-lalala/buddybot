package com.bot.buddybot.core.state;

import com.bot.buddybot.core.diary.service.DiaryService;
import com.bot.buddybot.core.mood.Mood;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;

public class Command {
    ChooseMoodState chooseMoodState;
    public ChooseActionState chooseActionState;
    WriteDiaryState writeDiaryState;
	ReadDiaryState readDiaryState;
	public GreetingsState greetingsState;
	CommandState currentState;
	Mood mood;

    public Command(){
        chooseMoodState = new ChooseMoodState(this);
		greetingsState = new GreetingsState(this);
		readDiaryState = new ReadDiaryState(this);
        currentState = greetingsState;
    }

    public String chooseMoodCommand(String pesan){
        return this.currentState.chooseMoodCommand(pesan);
    }

    public void setState(CommandState state){
        this.currentState = state;
    }
	
	public String chooseDiaryCommand(MessageEvent<TextMessageContent> messageEvent, 
									  DiaryService diaryService,
									  String pesan){
        return this.currentState.chooseDiaryCommand(messageEvent, diaryService, pesan);
    }
	
	public Boolean currentStateIsDiary(){
		if(currentState instanceof ReadDiaryState
		   || currentState instanceof WriteDiaryState){
			return true;
		}else{
			return false;
		}
	}

	public String help(){
		return this.currentState.help();
	}
	
	public String getQuickReplyHelp(){
		return this.currentState.getQuickReplyHelp();
	}
}
