package com.bot.buddybot.core.state;

import com.bot.buddybot.core.diary.service.DiaryService;
import com.bot.buddybot.core.mood.Mood;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import java.util.Arrays;
import java.util.List;

public class ChooseActionState implements CommandState{

    Command command;
	Mood mood;

    public ChooseActionState(Command command, Mood mood){
        this.command = command;
		this.mood = mood;
    }
	
	//return informations of available commands in String form
	@Override
	public String help(){
		String jawaban = mood.getHelp();
		return jawaban;
	}
	
	@Override
	public String getQuickReplyHelp(){
		return mood.getQuickReplyHelp();
	}
	
	//handle the input given by user
    @Override
    public String chooseMoodCommand(String pesan) {
		String jawaban;
		String[] changeStateInput = new String[] {"/gantimood", "/bacadiary", "/maunulis"};
		String[] behaviorInput = new String[] {mood.command[0], 
							mood.command[1], 
							mood.command[2]};
		List<String> changeStateInputList = Arrays.asList(changeStateInput);
		List<String> behaviorInputList = Arrays.asList(behaviorInput);
		if(pesan.equals("/help")){
			jawaban = help();
		}
		else if(behaviorInputList.contains(pesan)){
            jawaban = handleBehaviorInputs(pesan);
        }
		else if(changeStateInputList.contains(pesan)){
			jawaban = handleChangeStateInputs(pesan);
		}
		else{
			jawaban = "command tidak terdaftar";
		}
		return jawaban;
    }
	
	@Override
	public String chooseDiaryCommand(MessageEvent<TextMessageContent> messageEvent, 
									  DiaryService diaryService,
									  String pesan) {
        return "Kamu belum kasih tau buddybot sedang merasakan apa";
    }
	
	public String handleChangeStateInputs(String pesan){
		String jawaban;
		if(pesan.equals("/gantimood")){
			command.setState(this.command.chooseMoodState);
			jawaban = "ketik /help untuk lihat command";
		}else if(pesan.equals("/bacadiary")){
			command.setState(this.command.readDiaryState);
			jawaban = "Baik, silahkan membaca diary, "
					+"ketik /help untuk perintah lebih lanjut";
		}else{
			command.writeDiaryState = new WriteDiaryState(command,mood);
			command.setState(this.command.writeDiaryState);
			jawaban = "Silahkan ketik apa yang ingin ditulis, "
					+"setelah selesai ketik /selesai atau /bacaDiary. "
					+"Untuk informasi command lebih lanjut ketik /help";
		}
		return jawaban;
	}
	
	public String handleBehaviorInputs(String pesan) {
		String jawaban;
		if(pesan.equals(mood.command[0])){
            jawaban = mood.getQuotes();
        }else if(pesan.equals(mood.command[1])){
            jawaban = mood.getSaran();
        }else {
			jawaban = mood.getFotoKucing();
        }
		return jawaban;
	}
}