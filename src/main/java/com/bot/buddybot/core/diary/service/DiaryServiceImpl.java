package com.bot.buddybot.core.diary.service;

import com.bot.buddybot.core.diary.core.Diary;
import com.bot.buddybot.core.diary.repository.DiaryRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiaryServiceImpl implements DiaryService {
	
	@Autowired
	DiaryRepository dr;
	
	@Override
	public List<Diary> findAll(){
		return dr.findAll();
		}
	
	@Override
	public void erase(long id){
		dr.deleteById(id);
	}
	
	@Override
	public Diary register(Diary diary){
		return dr.save(diary);
	}
	
	@Override
	public List<Diary> findWithUserId(String type, String userId){
		if(type.equals("Semua")){
			return dr.findDiaryByUserId(userId);
		}else{
			return dr.findDiaryByMood(type, userId);
		}
	}
	
	@Override
	public Diary findWithMessageId(Long messageId){
		return dr.findDiaryByMessageId(messageId);
	}
	
	public boolean isEmpty(String userId, String type){
		if(type.equals("Semua")){
			if(dr.findDiaryByUserId(userId).size() != 0){
				return false;
			}
		}else{
			if(dr.findDiaryByMood(type, userId).size() != 0){
				return false;
			}
		}return true;
	}
}
