package com.bot.buddybot.core.diary.repository;

import com.bot.buddybot.core.diary.core.Diary;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DiaryRepository extends JpaRepository<Diary, Long> {
	@Query(value="SELECT * FROM diary WHERE user_id = ?1 ORDER BY date_created",
	nativeQuery = true)
	List<Diary> findDiaryByUserId(String userId);
	
	@Query(value="SELECT * FROM diary WHERE mood = ?1 AND user_id = ?2 ORDER BY date_created",
	nativeQuery = true)
	List<Diary> findDiaryByMood(String mood, String userId);
	
	@Query(value = "SELECT * FROM diary WHERE message_id = ?1", nativeQuery = true)
	Diary findDiaryByMessageId(Long messageId);
}
