package com.bot.buddybot.core.diary.service;

import com.bot.buddybot.core.diary.core.Diary;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface DiaryService {
	public List<Diary> findAll();
	
	public void erase(long id);
	
	public Diary register(Diary diary);
	
	public List<Diary> findWithUserId(String mood,String userId);
	
	public Diary findWithMessageId(Long messageId);
	
	public boolean isEmpty(String userId, String type);
}
