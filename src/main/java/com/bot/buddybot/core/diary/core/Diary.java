package com.bot.buddybot.core.diary.core;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "diary")
public class Diary {

	@Id
	@Column(name = "message_id", nullable = false)
	private long messageId;
	
	@Column(name = "user_id")
	private String userId;
	 
	@Column(name = "diary_text")
	private String diaryText;
	
	@Column(name = "mood")
	private String mood;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_created", 
			nullable = false,
			updatable = false,
			columnDefinition = "TIMESTAMP WITH TIME ZONE")
	private Date dateCreated;
	
	public Diary(long messageId, String userId, String diaryText, String mood){
		this.messageId = messageId;
		this.userId = userId;
		this.diaryText = diaryText;
		this.dateCreated = new Date();
		this.mood = mood;
	}
	
	public Diary(){
	}
	
	public String getDiaryText(){
		return diaryText;
	}
	
	public Long getMessageId(){
		return messageId;
	}
	
	public String getUserId(){
		return userId;
	}
	
	public String getMood(){
		return mood;
	}
	
	public Date getDateCreated(){
		return dateCreated;
	}
	
	public void setDiaryText(String diaryText){
		this.diaryText = diaryText;
	}
	
	public void setMood(String mood){
		this.mood = mood;
	}
	
	public void setUserId(String userId){
		this.userId = userId;
	}
	
	public Diary get(){
		return this;
	}

	public String toString(){
		return "diary id : " + messageId + " | " + mood + "\n" 
			   + "-----------------------------------------------\n"
		       + diaryText 
			   + "o---------------------------------------------o\n";
	}
}
