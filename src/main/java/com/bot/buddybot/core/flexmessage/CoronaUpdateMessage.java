package com.bot.buddybot.core.flexmessage;

import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONException;

public class CoronaUpdateMessage {
	
	static final String GREENPALETTE = "#59D1AD";
	static final String BLACKPALETTE = "#505A5C";
	static final String WHITEPALETTE = "#FFFFFF";
	String errorText = "terjadi error";

	public FlexMessage get(QuickReply quickReply){

	Box bodyBlock = createBodyBlock();
	Bubble bubble =
		Bubble.builder()
			.body(bodyBlock)
			.build();

	return FlexMessage.builder()
					.altText("corona report")
					.contents(bubble)
					.quickReply(quickReply)
					.build();
	}

	private Box createBodyBlock() {
	Text title = createText("Corona Update", FlexFontSize.XXXL, BLACKPALETTE,
			FlexAlign.CENTER, FlexMarginSize.MD);
	Separator separator = Separator.builder()
								.color(WHITEPALETTE)
								.margin(FlexMarginSize.XXL)
								.build();
	Box verticalContentBox = getContentBox();
	ArrayList<FlexComponent> list = new ArrayList<>(Arrays.asList(title,
			separator, verticalContentBox));
	Box verticalBox = Box.builder()
			.layout(FlexLayout.VERTICAL)
			.backgroundColor(GREENPALETTE)
			.borderColor(BLACKPALETTE)
			.borderWidth("3px")
			.cornerRadius("18px")
			.contents(list)
			.build();
	return verticalBox;
	}

	private Text createText(String content, FlexFontSize flexFontSize,
							String color, FlexAlign flexAlign,
							FlexMarginSize flexMarginSize){
		Text text =
		Text.builder()
			.text(content)
			.weight(TextWeight.BOLD)
			.size(flexFontSize)
			.color(color)
			.align(flexAlign)
			.margin(flexMarginSize)
			.build();
		return text;
	}
	
	private Box createHorizontalContent(String colOne, String colTwo){
		Text columnOne = createText(colOne, FlexFontSize.SM, BLACKPALETTE,
				FlexAlign.START, FlexMarginSize.NONE);
		Text columnTwo = createText(colTwo, FlexFontSize.SM, BLACKPALETTE,
				FlexAlign.END, FlexMarginSize.NONE);
		ArrayList<FlexComponent> list = new ArrayList<>(
				Arrays.asList(columnOne, columnTwo));
		return Box.builder()
			.layout(FlexLayout.HORIZONTAL)
			.contents(list)
			.build();
	}
	
	private Box getContentBox(){
		Text earth = createText("Earth", FlexFontSize.LG, BLACKPALETTE,
				FlexAlign.START, FlexMarginSize.XXL);
		Text indonesia = createText("Indonesia", FlexFontSize.LG, BLACKPALETTE,
				FlexAlign.START, FlexMarginSize.NONE);
		Separator separator = Separator.builder().color(WHITEPALETTE)
								.margin(FlexMarginSize.XXL)
								.build();
		String[] dataLocal = getLocalCoronaUpdate();
		Box confirmedRow = createHorizontalContent("Confirmed",
				changeNumberFormat(dataLocal[0]));
		Box deathsRow = createHorizontalContent("Deaths",
				changeNumberFormat(dataLocal[1]));
		Box recoveredRow = createHorizontalContent("Recovered",
				changeNumberFormat(dataLocal[2]));
		
		String[] dataGlobal = getGlobalCoronaUpdate();
		Box confirmedEarthRow = createHorizontalContent("Confirmed", dataGlobal[0]);
		Box deathsEarthRow = createHorizontalContent("Deaths", dataGlobal[1]);
		Box recoveredEarthRow = createHorizontalContent("Recovered", dataGlobal[2]);
		
		ArrayList<FlexComponent> list = new ArrayList<>(Arrays.asList(indonesia,
				confirmedRow, deathsRow, recoveredRow, separator, earth,
				confirmedEarthRow, deathsEarthRow, recoveredEarthRow));
		return Box.builder().layout(FlexLayout.VERTICAL)
			.margin(FlexMarginSize.XXL).spacing(FlexMarginSize.SM)
			.contents(list)
			.build();
	}
	
	private String[] getLocalCoronaUpdate(){
		try{
			HttpResponse<JsonNode> jsonResponse = Unirest
			.get("https://covid-19-coronavirus-statistics.p.rapidapi.com/v1/total?country=Indonesia")
			.header("X-RapidAPI-Host", 
			"covid-19-coronavirus-statistics.p.rapidapi.com")
			.header("X-RapidAPI-Key", 
			"2c4c0c55f4mshff6291822434cfep1de039jsne442d11ef893")
			.asJson();
			String confirmed = Integer.toString(jsonResponse.getBody()
			.getObject().getJSONObject("data").getInt("confirmed"));
			String deaths = Integer.toString(jsonResponse.getBody()
			.getObject().getJSONObject("data").getInt("deaths"));
			String recovered = Integer.toString(jsonResponse.getBody()
			.getObject().getJSONObject("data").getInt("recovered"));
			String[] data = new String[]{confirmed, deaths, recovered};
			return data;
		}catch(JSONException | UnirestException e){
			e.printStackTrace();
			return new String[]{errorText, errorText, errorText};
		}
	}
	
	private String[] getGlobalCoronaUpdate(){
		try{
			HttpResponse<JsonNode> jsonResponse = Unirest
			.get("https://coronavirus-monitor.p.rapidapi.com/coronavirus/worldstat.php")
			.header("x-rapidapi-host", 
			"coronavirus-monitor.p.rapidapi.com")
			.header("x-rapidapi-key", 
			"2c4c0c55f4mshff6291822434cfep1de039jsne442d11ef893")
			.asJson();
			String confirmed = jsonResponse.getBody().getObject()
					.getString("total_cases");
			String deaths = jsonResponse.getBody().getObject()
					.getString("total_deaths");
			String recovered = jsonResponse.getBody().getObject()
					.getString("total_recovered");
			String[] data = new String[]{confirmed, deaths, recovered};
			return data;
		}catch(JSONException | UnirestException e){
			e.printStackTrace();
			return new String[]{errorText, errorText, errorText};
		}
	}
	
	private String changeNumberFormat(String rawAmount){
		int indexTemp = 0;
		String output = "";
		for(int i = rawAmount.length()-1;i>=0;i--){
			if(indexTemp % 3 == 0 && indexTemp != 0){
				output = "," + output;
			}
			output = rawAmount.substring(i,i+1) + output;
			indexTemp++;
		}
		if(rawAmount.equals("terjadi error")){
			return rawAmount;
		}return output;
	}
}
