package com.bot.buddybot.core.flexmessage;

import com.bot.buddybot.core.diary.core.Diary;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Button.ButtonHeight;
import com.linecorp.bot.model.message.flex.component.Button.ButtonStyle;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DiaryListMessage {

	static final String GREENPALETTE = "#59D1AD";
	static final String BLACKPALETTE = "#505A5C";
	static final String WHITEPALETTE = "#FFFFFF";

	public FlexMessage get(List<Diary> diaryList, QuickReply quickReply){
	String userId = diaryList.get(0).getUserId();
	Box bodyBlock = createBodyBlock(diaryList);
	Box footerBlock = createFooterBlock(userId);
	Bubble bubble =
		Bubble.builder()
			.body(bodyBlock)
			.footer(footerBlock)
			.build();

	return FlexMessage.builder()
					.altText("lists of diary you made")
					.contents(bubble)
					.quickReply(quickReply)
					.build();
	}

	private Box createBodyBlock(List<Diary> diaryList) {
	Text title = createText("DIARY LIST", FlexFontSize.LG, GREENPALETTE,
			FlexAlign.START, FlexMarginSize.NONE, TextWeight.BOLD);
	Text subTitle = createText("All Diary", FlexFontSize.Md, WHITEPALETTE,
			FlexAlign.START, FlexMarginSize.MD, TextWeight.BOLD);
	ArrayList<FlexComponent> list = new ArrayList<>();
	list.add(title);
	list.add(subTitle);
	for(Diary diary : diaryList){
		Separator separator = Separator.builder()
									.color(WHITEPALETTE)
									.margin(FlexMarginSize.MD)
									.build();
		Box verticalContentBox = createVerticalContent(diary);
		list.add(separator);
		list.add(verticalContentBox);
	}
	Box verticalBox = Box.builder()
			.layout(FlexLayout.VERTICAL)
			.backgroundColor(BLACKPALETTE)
			.contents(list)
			.build();
	return verticalBox;
	}

	private Text createText(String content, FlexFontSize flexFontSize,
				String color, FlexAlign flexAlign,
				FlexMarginSize flexMarginSize, TextWeight tw){
		Text text =
		Text.builder()
			.text(content)
			.weight(tw)
			.size(flexFontSize)
			.color(color)
			.align(flexAlign)
			.margin(flexMarginSize)
			.build();
		return text;
	}
	
	private Box createHorizontalContent(String colOne, String colTwo){
		Text columnOne = createText(colOne, FlexFontSize.SM, WHITEPALETTE,
				FlexAlign.START, FlexMarginSize.NONE, TextWeight.REGULAR);
		Text columnTwo = createText(colTwo, FlexFontSize.SM, WHITEPALETTE,
				FlexAlign.END, FlexMarginSize.NONE, TextWeight.REGULAR);
		ArrayList<FlexComponent> list = new ArrayList<>(
				Arrays.asList(columnOne, columnTwo));
		return Box.builder()
			.layout(FlexLayout.HORIZONTAL)
			.contents(list)
			.build();
	}
	
	private Box createVerticalContent(Diary diary){
		MessageAction viewAction = new MessageAction("View Diary", 
		"/diaryview;"+diary.getMessageId());
		Button viewButton = Button
				.builder()
				.style(ButtonStyle.PRIMARY)
				.height(ButtonHeight.SMALL)
				.color(GREENPALETTE)
				.action(viewAction)
				.build();
		Box horizontalOne = createHorizontalContent("Mood :", diary.getMood());
		String dateCreated = new SimpleDateFormat("dd-MM-yyyy")
								.format(diary.getDateCreated());
		Box horizontalTwo = createHorizontalContent("Date :", dateCreated);	
		Box horizontalThree = createHorizontalContent("Text :", diary.getDiaryText());
		ArrayList<FlexComponent> list = new ArrayList<>(
			Arrays.asList(viewButton, horizontalOne, horizontalTwo, horizontalThree));
		return Box.builder()
			.layout(FlexLayout.VERTICAL)
			.margin(FlexMarginSize.LG)
			.spacing(FlexMarginSize.SM)
			.contents(list)
			.build();
		
	}
	
	private Box createFooterBlock(String userId){
		Text columnOne = createText("User ID", FlexFontSize.Md, BLACKPALETTE,
				FlexAlign.START, FlexMarginSize.NONE, TextWeight.REGULAR);
		Text columnTwo = createText(userId, FlexFontSize.Md, BLACKPALETTE,
				FlexAlign.START, FlexMarginSize.NONE, TextWeight.REGULAR);
		ArrayList<FlexComponent> list = new ArrayList<>(
				Arrays.asList(columnOne, columnTwo));
		return Box.builder()
			.layout(FlexLayout.HORIZONTAL)
			.backgroundColor(GREENPALETTE)
			.contents(list)
			.build();
	}
}
