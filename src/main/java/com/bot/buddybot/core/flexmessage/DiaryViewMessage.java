package com.bot.buddybot.core.flexmessage;

import com.bot.buddybot.core.diary.core.Diary;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Button.ButtonHeight;
import com.linecorp.bot.model.message.flex.component.Button.ButtonStyle;
import com.linecorp.bot.model.message.flex.component.FlexComponent;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.component.Text.TextWeight;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DiaryViewMessage {

	static final String GREENPALETTE = "#59D1AD";
	static final String BLACKPALETTE = "#505A5C";
	static final String WHITEPALETTE = "#FFFFFF";

	public FlexMessage get(Diary diary, QuickReply quickReply){
	Box bodyBlock = createBodyBlock(diary);
	Bubble bubble =
		Bubble.builder()
			.body(bodyBlock)
			.build();

	return FlexMessage.builder()
					.altText("details of the diary you made")
					.contents(bubble)
					.quickReply(quickReply)
					.build();
	}

	private Box createBodyBlock(Diary diary) {
	Text title = createText("DIARY DETAIL", FlexFontSize.LG, BLACKPALETTE,
			FlexAlign.START, FlexMarginSize.NONE, TextWeight.BOLD, false);
	String dateCreated = new SimpleDateFormat("dd-MM-yyyy").format(diary.getDateCreated());
	Box horizontalSubTitle = createHorizontalContent(diary.getMood(), dateCreated);
	Separator separator = Separator.builder()
									.color(WHITEPALETTE)
									.margin(FlexMarginSize.MD)
									.build();
	Text isi = createText(diary.getDiaryText(), FlexFontSize.Md, BLACKPALETTE,
			FlexAlign.START, FlexMarginSize.MD, TextWeight.REGULAR, true);
	Button deleteAction = Button.builder()
		.style(ButtonStyle.PRIMARY)
		.height(ButtonHeight.SMALL)
		.color(BLACKPALETTE)
		.margin(FlexMarginSize.MD)
		.action(new MessageAction("Delete Diary", "/delete["+diary.getMessageId()+"]"))
		.build();
	ArrayList<FlexComponent> list = new ArrayList<>(
	Arrays.asList(title, horizontalSubTitle, separator, isi, deleteAction));
	Box verticalBox = Box.builder()
			.layout(FlexLayout.VERTICAL)
			.backgroundColor(GREENPALETTE)
			.contents(list)
			.build();
	return verticalBox;
	}

	private Text createText(String content, FlexFontSize flexFontSize,
							String color, FlexAlign flexAlign,
							FlexMarginSize flexMarginSize, 
							TextWeight tw, boolean wrapped){
		Text text =
		Text.builder()
			.text(content)
			.weight(tw)
			.size(flexFontSize)
			.color(color)
			.align(flexAlign)
			.margin(flexMarginSize)
			.wrap(wrapped)
			.build();
		return text;
	}
	
	private Box createHorizontalContent(String colOne, String colTwo){
		Text columnOne = createText(colOne, FlexFontSize.Md, BLACKPALETTE,
				FlexAlign.START, FlexMarginSize.MD, TextWeight.BOLD, false);
		Text columnTwo = createText(colTwo, FlexFontSize.Md, BLACKPALETTE,
				FlexAlign.END, FlexMarginSize.MD, TextWeight.BOLD, false);
		ArrayList<FlexComponent> list = new ArrayList<>(
				Arrays.asList(columnOne, columnTwo));
		return Box.builder()
			.layout(FlexLayout.HORIZONTAL)
			.contents(list)
			.margin(FlexMarginSize.MD)
			.build();
	}
}
