package com.bot.buddybot.controller;

import com.bot.buddybot.core.diary.core.Diary;
import com.bot.buddybot.core.diary.service.DiaryService;
import com.bot.buddybot.core.flexmessage.CoronaUpdateMessage;
import com.bot.buddybot.core.flexmessage.DiaryListMessage;
import com.bot.buddybot.core.flexmessage.DiaryViewMessage;
import com.bot.buddybot.core.mood.Mood;
import com.bot.buddybot.core.scheduler.Scheduler;
import com.bot.buddybot.core.state.Command;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.Broadcast;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.action.URIAction.AltUri;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TemplateMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import com.linecorp.bot.model.message.quickreply.QuickReplyItem;
import com.linecorp.bot.model.message.template.CarouselColumn;
import com.linecorp.bot.model.message.template.CarouselTemplate;
import com.linecorp.bot.model.message.template.ImageCarouselColumn;
import com.linecorp.bot.model.message.template.ImageCarouselTemplate;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;

@LineMessageHandler
public class BuddyBotController {

	@Autowired
	private LineMessagingClient lineMessagingClient;

	@Autowired
	private DiaryService diaryService;

	public Mood mood;
	public Command command;
	public static Map<String, Command> state_manager = new HashMap<>();
	public Scheduler jadwal = new Scheduler(this);

	@EventMapping
	public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent){
		String userId = messageEvent.getSource().getUserId();
		assignCommandToCurrentState(userId);
		String pesan = messageEvent.getMessage().getText().toLowerCase();

		String jawaban = getResponseBasedOnStates(messageEvent, diaryService, pesan);
		ArrayList<Message> jawabanMessages = new ArrayList<>();
		String replyToken = messageEvent.getReplyToken();

		if(pesan.equals("/halo") && !jawaban.equals("command tidak terdaftar")){
			CoronaUpdateMessage coronaMessage = new CoronaUpdateMessage();
			jawabanMessages.add(coronaMessage.get(generateQuickReplyBasedOnState()));
		}
		
		jawabanMessages.add(convertStringToMessage(jawaban));
		balasChat(replyToken, jawabanMessages);
	}
	
	private void balasChat(String replyToken, List<Message> jawaban){
		try{
			lineMessagingClient
					.replyMessage(new ReplyMessage(replyToken, jawaban))
					.get();
		}catch(InterruptedException | ExecutionException e){
			Logger.getLogger(e.getMessage());
		}catch(NullPointerException e){
			e.printStackTrace();
		}
	}

	private void assignCommandToCurrentState(String userId){
		if(state_manager.containsKey(userId)){
			command = state_manager.get(userId);
		}else{
			command = new Command();
			state_manager.put(userId, command);
		}
	}

	private String getResponseBasedOnStates(MessageEvent<TextMessageContent> messageEvent,
						DiaryService diaryService,
						String pesan){
		String jawaban;
		if(command.currentStateIsDiary()){
			jawaban = command.chooseDiaryCommand(messageEvent,diaryService, pesan);
		}
		else{
			jawaban = command.chooseMoodCommand(pesan);
		}
		return jawaban;
	}

	public Message convertStringToMessage(String jawaban){
		if(jawaban.contains("kucingflag")){
			String jawabanAsli = jawaban.substring(10);
			String[] fotoList = jawabanAsli.split(",");
			return generateCatImagesCarousel(fotoList);
		}else if(jawaban.contains("refreshflag")) {
			String jawabanLink = jawaban.substring(11);
			String[] listSaran = jawabanLink.split(",");
			return generateCarouselTemplate(listSaran);
		}else if(jawaban.contains("diaryflag")) {
			return handleDiaryFlagInput(jawaban.substring(9));
		}else{
			return new TextMessage(jawaban, generateQuickReplyBasedOnState());
		}
	}
	
	private Message handleDiaryFlagInput(String input){
		String[] jawabanParsed = input.split(";");
		try{
			if(jawabanParsed[1].equals("View")){
				Long msgId = Long.parseLong(jawabanParsed[2]);
				Diary diary = diaryService.findWithMessageId(msgId);
				DiaryViewMessage viewDiary = new DiaryViewMessage();
				return viewDiary.get(diary, generateQuickReplyBasedOnState());
			}else{
				List<Diary> list = diaryService
				.findWithUserId(jawabanParsed[1], jawabanParsed[0]);
				DiaryListMessage listDiary = new DiaryListMessage();
				return listDiary.get(list, generateQuickReplyBasedOnState());
			}
		}catch(Exception e){
			return new TextMessage("View diary gagal, coba cek apakah input benar",
			generateQuickReplyBasedOnState());
		}
	}
	
	private TemplateMessage generateCatImagesCarousel(String[] fotoList){
		try{
			List<ImageCarouselColumn> listCarousel = new ArrayList<>();
			for(int i = 0;i<fotoList.length;i++){
				URI uriPath = new URI(fotoList[i]);
				AltUri altUri = new AltUri(uriPath);
				URIAction uriAction = new URIAction("go to URL", uriPath, altUri);
				listCarousel.add(new ImageCarouselColumn(uriPath, uriAction));
			}
			ImageCarouselTemplate catCarousel = new ImageCarouselTemplate(listCarousel);
			TemplateMessage templateMessage = TemplateMessage.builder()
					.quickReply(generateQuickReplyBasedOnState())
					.altText("Cat Images")
					.template(catCarousel)
					.build();
			return templateMessage;
		}catch(URISyntaxException e){
			return TemplateMessage.builder().altText("Error").build();
		}
	}

	private TemplateMessage generateCarouselTemplate(String[] listSaran){
		try{
			List<CarouselColumn> listCarousel = new ArrayList<>();
			for (int i=0;i<listSaran.length;i++){
				String[] descSaran = listSaran[i].split(";");

				URI uriPath = new URI(descSaran[0]);
				AltUri altUri = new AltUri(uriPath);
				URIAction uriAction = new URIAction("Go to URL", uriPath, altUri);

				String title = descSaran[1];
				String text = descSaran[2];

				listCarousel.add(new CarouselColumn(uriPath, title,
						text, Collections.singletonList(uriAction)));
			}
			CarouselTemplate placesCarousel = new CarouselTemplate(listCarousel);
			TemplateMessage templateMessage = TemplateMessage.builder()
					.quickReply(generateQuickReplyBasedOnState())
					.altText("Refreshing Places")
					.template(placesCarousel)
					.build();
			return templateMessage;
		}catch(URISyntaxException e){
			return TemplateMessage.builder().altText("Error").build();
		}
	}

	public void broadcastReply(String str) {
		Message text = null;
		try {
			text = convertStringToMessage(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Broadcast message = new Broadcast(text);
		try {
			lineMessagingClient.broadcast(message).get();
		}catch(InterruptedException | ExecutionException e){
			Logger.getLogger(e.getMessage());
		}
	}
	
	public QuickReply generateQuickReplyBasedOnState(){
		try{
			String[] quickReplyCommand = command.getQuickReplyHelp().split(" ");
			List<QuickReplyItem> quickReplyItems = new ArrayList<>();
			
			for(String quickReply : quickReplyCommand){
				URI uriPath = new URI("https://cdn0.iconfinder.com/data/icons/mathematical-symbols-2-colored/640/cc_slash2-512.png");
				MessageAction messageAction = new MessageAction(quickReply,
						"/"+quickReply.toLowerCase());
				QuickReplyItem quickReplyItem = QuickReplyItem.builder()
									  .action(messageAction)
									  .imageUrl(uriPath)
									  .build();
				quickReplyItems.add(quickReplyItem);
			}
			QuickReply quickReply = QuickReply.builder().items(quickReplyItems).build();
			return quickReply;
		}catch(URISyntaxException e){
			QuickReplyItem quickReplyItem = QuickReplyItem.builder()
			.action(new MessageAction("error", "/error")).build();
			List<QuickReplyItem> quickReplyItems = new ArrayList<>();
			quickReplyItems.add(quickReplyItem);
			return QuickReply.builder().items(quickReplyItems).build();
		}
	}
}
